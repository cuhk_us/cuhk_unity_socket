// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: stereo.proto

#ifndef PROTOBUF_stereo_2eproto__INCLUDED
#define PROTOBUF_stereo_2eproto__INCLUDED

#include <string>

#include <google/protobuf/stubs/common.h>

#if GOOGLE_PROTOBUF_VERSION < 2006000
#error This file was generated by a newer version of protoc which is
#error incompatible with your Protocol Buffer headers.  Please update
#error your headers.
#endif
#if 2006001 < GOOGLE_PROTOBUF_MIN_PROTOC_VERSION
#error This file was generated by an older version of protoc which is
#error incompatible with your Protocol Buffer headers.  Please
#error regenerate this file with a newer version of protoc.
#endif

#include <google/protobuf/generated_message_util.h>
#include <google/protobuf/message.h>
#include <google/protobuf/repeated_field.h>
#include <google/protobuf/extension_set.h>
#include <google/protobuf/unknown_field_set.h>
// @@protoc_insertion_point(includes)

namespace nus_protocol {

// Internal implementation detail -- do not call these.
void  protobuf_AddDesc_stereo_2eproto();
void protobuf_AssignDesc_stereo_2eproto();
void protobuf_ShutdownFile_stereo_2eproto();

class Stereo;

// ===================================================================

class Stereo : public ::google::protobuf::Message {
 public:
  Stereo();
  virtual ~Stereo();

  Stereo(const Stereo& from);

  inline Stereo& operator=(const Stereo& from) {
    CopyFrom(from);
    return *this;
  }

  inline const ::google::protobuf::UnknownFieldSet& unknown_fields() const {
    return _unknown_fields_;
  }

  inline ::google::protobuf::UnknownFieldSet* mutable_unknown_fields() {
    return &_unknown_fields_;
  }

  static const ::google::protobuf::Descriptor* descriptor();
  static const Stereo& default_instance();

  void Swap(Stereo* other);

  // implements Message ----------------------------------------------

  Stereo* New() const;
  void CopyFrom(const ::google::protobuf::Message& from);
  void MergeFrom(const ::google::protobuf::Message& from);
  void CopyFrom(const Stereo& from);
  void MergeFrom(const Stereo& from);
  void Clear();
  bool IsInitialized() const;

  int ByteSize() const;
  bool MergePartialFromCodedStream(
      ::google::protobuf::io::CodedInputStream* input);
  void SerializeWithCachedSizes(
      ::google::protobuf::io::CodedOutputStream* output) const;
  ::google::protobuf::uint8* SerializeWithCachedSizesToArray(::google::protobuf::uint8* output) const;
  int GetCachedSize() const { return _cached_size_; }
  private:
  void SharedCtor();
  void SharedDtor();
  void SetCachedSize(int size) const;
  public:
  ::google::protobuf::Metadata GetMetadata() const;

  // nested types ----------------------------------------------------

  // accessors -------------------------------------------------------

  // optional string name = 1;
  inline bool has_name() const;
  inline void clear_name();
  static const int kNameFieldNumber = 1;
  inline const ::std::string& name() const;
  inline void set_name(const ::std::string& value);
  inline void set_name(const char* value);
  inline void set_name(const char* value, size_t size);
  inline ::std::string* mutable_name();
  inline ::std::string* release_name();
  inline void set_allocated_name(::std::string* name);

  // optional uint32 id = 2;
  inline bool has_id() const;
  inline void clear_id();
  static const int kIdFieldNumber = 2;
  inline ::google::protobuf::uint32 id() const;
  inline void set_id(::google::protobuf::uint32 value);

  // optional uint32 seq = 3;
  inline bool has_seq() const;
  inline void clear_seq();
  static const int kSeqFieldNumber = 3;
  inline ::google::protobuf::uint32 seq() const;
  inline void set_seq(::google::protobuf::uint32 value);

  // optional float stamp = 4;
  inline bool has_stamp() const;
  inline void clear_stamp();
  static const int kStampFieldNumber = 4;
  inline float stamp() const;
  inline void set_stamp(float value);

  // optional float baseline = 5;
  inline bool has_baseline() const;
  inline void clear_baseline();
  static const int kBaselineFieldNumber = 5;
  inline float baseline() const;
  inline void set_baseline(float value);

  // optional float vertical_fov = 6;
  inline bool has_vertical_fov() const;
  inline void clear_vertical_fov();
  static const int kVerticalFovFieldNumber = 6;
  inline float vertical_fov() const;
  inline void set_vertical_fov(float value);

  // optional bytes image_left_data = 7;
  inline bool has_image_left_data() const;
  inline void clear_image_left_data();
  static const int kImageLeftDataFieldNumber = 7;
  inline const ::std::string& image_left_data() const;
  inline void set_image_left_data(const ::std::string& value);
  inline void set_image_left_data(const char* value);
  inline void set_image_left_data(const void* value, size_t size);
  inline ::std::string* mutable_image_left_data();
  inline ::std::string* release_image_left_data();
  inline void set_allocated_image_left_data(::std::string* image_left_data);

  // optional bytes image_right_data = 8;
  inline bool has_image_right_data() const;
  inline void clear_image_right_data();
  static const int kImageRightDataFieldNumber = 8;
  inline const ::std::string& image_right_data() const;
  inline void set_image_right_data(const ::std::string& value);
  inline void set_image_right_data(const char* value);
  inline void set_image_right_data(const void* value, size_t size);
  inline ::std::string* mutable_image_right_data();
  inline ::std::string* release_image_right_data();
  inline void set_allocated_image_right_data(::std::string* image_right_data);

  // @@protoc_insertion_point(class_scope:nus_protocol.Stereo)
 private:
  inline void set_has_name();
  inline void clear_has_name();
  inline void set_has_id();
  inline void clear_has_id();
  inline void set_has_seq();
  inline void clear_has_seq();
  inline void set_has_stamp();
  inline void clear_has_stamp();
  inline void set_has_baseline();
  inline void clear_has_baseline();
  inline void set_has_vertical_fov();
  inline void clear_has_vertical_fov();
  inline void set_has_image_left_data();
  inline void clear_has_image_left_data();
  inline void set_has_image_right_data();
  inline void clear_has_image_right_data();

  ::google::protobuf::UnknownFieldSet _unknown_fields_;

  ::google::protobuf::uint32 _has_bits_[1];
  mutable int _cached_size_;
  ::std::string* name_;
  ::google::protobuf::uint32 id_;
  ::google::protobuf::uint32 seq_;
  float stamp_;
  float baseline_;
  ::std::string* image_left_data_;
  ::std::string* image_right_data_;
  float vertical_fov_;
  friend void  protobuf_AddDesc_stereo_2eproto();
  friend void protobuf_AssignDesc_stereo_2eproto();
  friend void protobuf_ShutdownFile_stereo_2eproto();

  void InitAsDefaultInstance();
  static Stereo* default_instance_;
};
// ===================================================================


// ===================================================================

// Stereo

// optional string name = 1;
inline bool Stereo::has_name() const {
  return (_has_bits_[0] & 0x00000001u) != 0;
}
inline void Stereo::set_has_name() {
  _has_bits_[0] |= 0x00000001u;
}
inline void Stereo::clear_has_name() {
  _has_bits_[0] &= ~0x00000001u;
}
inline void Stereo::clear_name() {
  if (name_ != &::google::protobuf::internal::GetEmptyStringAlreadyInited()) {
    name_->clear();
  }
  clear_has_name();
}
inline const ::std::string& Stereo::name() const {
  // @@protoc_insertion_point(field_get:nus_protocol.Stereo.name)
  return *name_;
}
inline void Stereo::set_name(const ::std::string& value) {
  set_has_name();
  if (name_ == &::google::protobuf::internal::GetEmptyStringAlreadyInited()) {
    name_ = new ::std::string;
  }
  name_->assign(value);
  // @@protoc_insertion_point(field_set:nus_protocol.Stereo.name)
}
inline void Stereo::set_name(const char* value) {
  set_has_name();
  if (name_ == &::google::protobuf::internal::GetEmptyStringAlreadyInited()) {
    name_ = new ::std::string;
  }
  name_->assign(value);
  // @@protoc_insertion_point(field_set_char:nus_protocol.Stereo.name)
}
inline void Stereo::set_name(const char* value, size_t size) {
  set_has_name();
  if (name_ == &::google::protobuf::internal::GetEmptyStringAlreadyInited()) {
    name_ = new ::std::string;
  }
  name_->assign(reinterpret_cast<const char*>(value), size);
  // @@protoc_insertion_point(field_set_pointer:nus_protocol.Stereo.name)
}
inline ::std::string* Stereo::mutable_name() {
  set_has_name();
  if (name_ == &::google::protobuf::internal::GetEmptyStringAlreadyInited()) {
    name_ = new ::std::string;
  }
  // @@protoc_insertion_point(field_mutable:nus_protocol.Stereo.name)
  return name_;
}
inline ::std::string* Stereo::release_name() {
  clear_has_name();
  if (name_ == &::google::protobuf::internal::GetEmptyStringAlreadyInited()) {
    return NULL;
  } else {
    ::std::string* temp = name_;
    name_ = const_cast< ::std::string*>(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
    return temp;
  }
}
inline void Stereo::set_allocated_name(::std::string* name) {
  if (name_ != &::google::protobuf::internal::GetEmptyStringAlreadyInited()) {
    delete name_;
  }
  if (name) {
    set_has_name();
    name_ = name;
  } else {
    clear_has_name();
    name_ = const_cast< ::std::string*>(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
  }
  // @@protoc_insertion_point(field_set_allocated:nus_protocol.Stereo.name)
}

// optional uint32 id = 2;
inline bool Stereo::has_id() const {
  return (_has_bits_[0] & 0x00000002u) != 0;
}
inline void Stereo::set_has_id() {
  _has_bits_[0] |= 0x00000002u;
}
inline void Stereo::clear_has_id() {
  _has_bits_[0] &= ~0x00000002u;
}
inline void Stereo::clear_id() {
  id_ = 0u;
  clear_has_id();
}
inline ::google::protobuf::uint32 Stereo::id() const {
  // @@protoc_insertion_point(field_get:nus_protocol.Stereo.id)
  return id_;
}
inline void Stereo::set_id(::google::protobuf::uint32 value) {
  set_has_id();
  id_ = value;
  // @@protoc_insertion_point(field_set:nus_protocol.Stereo.id)
}

// optional uint32 seq = 3;
inline bool Stereo::has_seq() const {
  return (_has_bits_[0] & 0x00000004u) != 0;
}
inline void Stereo::set_has_seq() {
  _has_bits_[0] |= 0x00000004u;
}
inline void Stereo::clear_has_seq() {
  _has_bits_[0] &= ~0x00000004u;
}
inline void Stereo::clear_seq() {
  seq_ = 0u;
  clear_has_seq();
}
inline ::google::protobuf::uint32 Stereo::seq() const {
  // @@protoc_insertion_point(field_get:nus_protocol.Stereo.seq)
  return seq_;
}
inline void Stereo::set_seq(::google::protobuf::uint32 value) {
  set_has_seq();
  seq_ = value;
  // @@protoc_insertion_point(field_set:nus_protocol.Stereo.seq)
}

// optional float stamp = 4;
inline bool Stereo::has_stamp() const {
  return (_has_bits_[0] & 0x00000008u) != 0;
}
inline void Stereo::set_has_stamp() {
  _has_bits_[0] |= 0x00000008u;
}
inline void Stereo::clear_has_stamp() {
  _has_bits_[0] &= ~0x00000008u;
}
inline void Stereo::clear_stamp() {
  stamp_ = 0;
  clear_has_stamp();
}
inline float Stereo::stamp() const {
  // @@protoc_insertion_point(field_get:nus_protocol.Stereo.stamp)
  return stamp_;
}
inline void Stereo::set_stamp(float value) {
  set_has_stamp();
  stamp_ = value;
  // @@protoc_insertion_point(field_set:nus_protocol.Stereo.stamp)
}

// optional float baseline = 5;
inline bool Stereo::has_baseline() const {
  return (_has_bits_[0] & 0x00000010u) != 0;
}
inline void Stereo::set_has_baseline() {
  _has_bits_[0] |= 0x00000010u;
}
inline void Stereo::clear_has_baseline() {
  _has_bits_[0] &= ~0x00000010u;
}
inline void Stereo::clear_baseline() {
  baseline_ = 0;
  clear_has_baseline();
}
inline float Stereo::baseline() const {
  // @@protoc_insertion_point(field_get:nus_protocol.Stereo.baseline)
  return baseline_;
}
inline void Stereo::set_baseline(float value) {
  set_has_baseline();
  baseline_ = value;
  // @@protoc_insertion_point(field_set:nus_protocol.Stereo.baseline)
}

// optional float vertical_fov = 6;
inline bool Stereo::has_vertical_fov() const {
  return (_has_bits_[0] & 0x00000020u) != 0;
}
inline void Stereo::set_has_vertical_fov() {
  _has_bits_[0] |= 0x00000020u;
}
inline void Stereo::clear_has_vertical_fov() {
  _has_bits_[0] &= ~0x00000020u;
}
inline void Stereo::clear_vertical_fov() {
  vertical_fov_ = 0;
  clear_has_vertical_fov();
}
inline float Stereo::vertical_fov() const {
  // @@protoc_insertion_point(field_get:nus_protocol.Stereo.vertical_fov)
  return vertical_fov_;
}
inline void Stereo::set_vertical_fov(float value) {
  set_has_vertical_fov();
  vertical_fov_ = value;
  // @@protoc_insertion_point(field_set:nus_protocol.Stereo.vertical_fov)
}

// optional bytes image_left_data = 7;
inline bool Stereo::has_image_left_data() const {
  return (_has_bits_[0] & 0x00000040u) != 0;
}
inline void Stereo::set_has_image_left_data() {
  _has_bits_[0] |= 0x00000040u;
}
inline void Stereo::clear_has_image_left_data() {
  _has_bits_[0] &= ~0x00000040u;
}
inline void Stereo::clear_image_left_data() {
  if (image_left_data_ != &::google::protobuf::internal::GetEmptyStringAlreadyInited()) {
    image_left_data_->clear();
  }
  clear_has_image_left_data();
}
inline const ::std::string& Stereo::image_left_data() const {
  // @@protoc_insertion_point(field_get:nus_protocol.Stereo.image_left_data)
  return *image_left_data_;
}
inline void Stereo::set_image_left_data(const ::std::string& value) {
  set_has_image_left_data();
  if (image_left_data_ == &::google::protobuf::internal::GetEmptyStringAlreadyInited()) {
    image_left_data_ = new ::std::string;
  }
  image_left_data_->assign(value);
  // @@protoc_insertion_point(field_set:nus_protocol.Stereo.image_left_data)
}
inline void Stereo::set_image_left_data(const char* value) {
  set_has_image_left_data();
  if (image_left_data_ == &::google::protobuf::internal::GetEmptyStringAlreadyInited()) {
    image_left_data_ = new ::std::string;
  }
  image_left_data_->assign(value);
  // @@protoc_insertion_point(field_set_char:nus_protocol.Stereo.image_left_data)
}
inline void Stereo::set_image_left_data(const void* value, size_t size) {
  set_has_image_left_data();
  if (image_left_data_ == &::google::protobuf::internal::GetEmptyStringAlreadyInited()) {
    image_left_data_ = new ::std::string;
  }
  image_left_data_->assign(reinterpret_cast<const char*>(value), size);
  // @@protoc_insertion_point(field_set_pointer:nus_protocol.Stereo.image_left_data)
}
inline ::std::string* Stereo::mutable_image_left_data() {
  set_has_image_left_data();
  if (image_left_data_ == &::google::protobuf::internal::GetEmptyStringAlreadyInited()) {
    image_left_data_ = new ::std::string;
  }
  // @@protoc_insertion_point(field_mutable:nus_protocol.Stereo.image_left_data)
  return image_left_data_;
}
inline ::std::string* Stereo::release_image_left_data() {
  clear_has_image_left_data();
  if (image_left_data_ == &::google::protobuf::internal::GetEmptyStringAlreadyInited()) {
    return NULL;
  } else {
    ::std::string* temp = image_left_data_;
    image_left_data_ = const_cast< ::std::string*>(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
    return temp;
  }
}
inline void Stereo::set_allocated_image_left_data(::std::string* image_left_data) {
  if (image_left_data_ != &::google::protobuf::internal::GetEmptyStringAlreadyInited()) {
    delete image_left_data_;
  }
  if (image_left_data) {
    set_has_image_left_data();
    image_left_data_ = image_left_data;
  } else {
    clear_has_image_left_data();
    image_left_data_ = const_cast< ::std::string*>(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
  }
  // @@protoc_insertion_point(field_set_allocated:nus_protocol.Stereo.image_left_data)
}

// optional bytes image_right_data = 8;
inline bool Stereo::has_image_right_data() const {
  return (_has_bits_[0] & 0x00000080u) != 0;
}
inline void Stereo::set_has_image_right_data() {
  _has_bits_[0] |= 0x00000080u;
}
inline void Stereo::clear_has_image_right_data() {
  _has_bits_[0] &= ~0x00000080u;
}
inline void Stereo::clear_image_right_data() {
  if (image_right_data_ != &::google::protobuf::internal::GetEmptyStringAlreadyInited()) {
    image_right_data_->clear();
  }
  clear_has_image_right_data();
}
inline const ::std::string& Stereo::image_right_data() const {
  // @@protoc_insertion_point(field_get:nus_protocol.Stereo.image_right_data)
  return *image_right_data_;
}
inline void Stereo::set_image_right_data(const ::std::string& value) {
  set_has_image_right_data();
  if (image_right_data_ == &::google::protobuf::internal::GetEmptyStringAlreadyInited()) {
    image_right_data_ = new ::std::string;
  }
  image_right_data_->assign(value);
  // @@protoc_insertion_point(field_set:nus_protocol.Stereo.image_right_data)
}
inline void Stereo::set_image_right_data(const char* value) {
  set_has_image_right_data();
  if (image_right_data_ == &::google::protobuf::internal::GetEmptyStringAlreadyInited()) {
    image_right_data_ = new ::std::string;
  }
  image_right_data_->assign(value);
  // @@protoc_insertion_point(field_set_char:nus_protocol.Stereo.image_right_data)
}
inline void Stereo::set_image_right_data(const void* value, size_t size) {
  set_has_image_right_data();
  if (image_right_data_ == &::google::protobuf::internal::GetEmptyStringAlreadyInited()) {
    image_right_data_ = new ::std::string;
  }
  image_right_data_->assign(reinterpret_cast<const char*>(value), size);
  // @@protoc_insertion_point(field_set_pointer:nus_protocol.Stereo.image_right_data)
}
inline ::std::string* Stereo::mutable_image_right_data() {
  set_has_image_right_data();
  if (image_right_data_ == &::google::protobuf::internal::GetEmptyStringAlreadyInited()) {
    image_right_data_ = new ::std::string;
  }
  // @@protoc_insertion_point(field_mutable:nus_protocol.Stereo.image_right_data)
  return image_right_data_;
}
inline ::std::string* Stereo::release_image_right_data() {
  clear_has_image_right_data();
  if (image_right_data_ == &::google::protobuf::internal::GetEmptyStringAlreadyInited()) {
    return NULL;
  } else {
    ::std::string* temp = image_right_data_;
    image_right_data_ = const_cast< ::std::string*>(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
    return temp;
  }
}
inline void Stereo::set_allocated_image_right_data(::std::string* image_right_data) {
  if (image_right_data_ != &::google::protobuf::internal::GetEmptyStringAlreadyInited()) {
    delete image_right_data_;
  }
  if (image_right_data) {
    set_has_image_right_data();
    image_right_data_ = image_right_data;
  } else {
    clear_has_image_right_data();
    image_right_data_ = const_cast< ::std::string*>(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
  }
  // @@protoc_insertion_point(field_set_allocated:nus_protocol.Stereo.image_right_data)
}


// @@protoc_insertion_point(namespace_scope)

}  // namespace nus_protocol

#ifndef SWIG
namespace google {
namespace protobuf {


}  // namespace google
}  // namespace protobuf
#endif  // SWIG

// @@protoc_insertion_point(global_scope)

#endif  // PROTOBUF_stereo_2eproto__INCLUDED
