// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: laser_scan.proto

#ifndef PROTOBUF_INCLUDED_laser_5fscan_2eproto
#define PROTOBUF_INCLUDED_laser_5fscan_2eproto

#include <limits>
#include <string>

#include <google/protobuf/port_def.inc>
#if PROTOBUF_VERSION < 3007000
#error This file was generated by a newer version of protoc which is
#error incompatible with your Protocol Buffer headers. Please update
#error your headers.
#endif
#if 3007000 < PROTOBUF_MIN_PROTOC_VERSION
#error This file was generated by an older version of protoc which is
#error incompatible with your Protocol Buffer headers. Please
#error regenerate this file with a newer version of protoc.
#endif

#include <google/protobuf/port_undef.inc>
#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/arena.h>
#include <google/protobuf/arenastring.h>
#include <google/protobuf/generated_message_table_driven.h>
#include <google/protobuf/generated_message_util.h>
#include <google/protobuf/inlined_string_field.h>
#include <google/protobuf/metadata.h>
#include <google/protobuf/message.h>
#include <google/protobuf/repeated_field.h>  // IWYU pragma: export
#include <google/protobuf/extension_set.h>  // IWYU pragma: export
#include <google/protobuf/unknown_field_set.h>
// @@protoc_insertion_point(includes)
#include <google/protobuf/port_def.inc>
#define PROTOBUF_INTERNAL_EXPORT_laser_5fscan_2eproto

// Internal implementation detail -- do not use these members.
struct TableStruct_laser_5fscan_2eproto {
  static const ::google::protobuf::internal::ParseTableField entries[]
    PROTOBUF_SECTION_VARIABLE(protodesc_cold);
  static const ::google::protobuf::internal::AuxillaryParseTableField aux[]
    PROTOBUF_SECTION_VARIABLE(protodesc_cold);
  static const ::google::protobuf::internal::ParseTable schema[1]
    PROTOBUF_SECTION_VARIABLE(protodesc_cold);
  static const ::google::protobuf::internal::FieldMetadata field_metadata[];
  static const ::google::protobuf::internal::SerializationTable serialization_table[];
  static const ::google::protobuf::uint32 offsets[];
};
void AddDescriptors_laser_5fscan_2eproto();
namespace nus_protocol {
class LaserScan;
class LaserScanDefaultTypeInternal;
extern LaserScanDefaultTypeInternal _LaserScan_default_instance_;
}  // namespace nus_protocol
namespace google {
namespace protobuf {
template<> ::nus_protocol::LaserScan* Arena::CreateMaybeMessage<::nus_protocol::LaserScan>(Arena*);
}  // namespace protobuf
}  // namespace google
namespace nus_protocol {

// ===================================================================

class LaserScan final :
    public ::google::protobuf::Message /* @@protoc_insertion_point(class_definition:nus_protocol.LaserScan) */ {
 public:
  LaserScan();
  virtual ~LaserScan();

  LaserScan(const LaserScan& from);

  inline LaserScan& operator=(const LaserScan& from) {
    CopyFrom(from);
    return *this;
  }
  #if LANG_CXX11
  LaserScan(LaserScan&& from) noexcept
    : LaserScan() {
    *this = ::std::move(from);
  }

  inline LaserScan& operator=(LaserScan&& from) noexcept {
    if (GetArenaNoVirtual() == from.GetArenaNoVirtual()) {
      if (this != &from) InternalSwap(&from);
    } else {
      CopyFrom(from);
    }
    return *this;
  }
  #endif
  static const ::google::protobuf::Descriptor* descriptor() {
    return default_instance().GetDescriptor();
  }
  static const LaserScan& default_instance();

  static void InitAsDefaultInstance();  // FOR INTERNAL USE ONLY
  static inline const LaserScan* internal_default_instance() {
    return reinterpret_cast<const LaserScan*>(
               &_LaserScan_default_instance_);
  }
  static constexpr int kIndexInFileMessages =
    0;

  void Swap(LaserScan* other);
  friend void swap(LaserScan& a, LaserScan& b) {
    a.Swap(&b);
  }

  // implements Message ----------------------------------------------

  inline LaserScan* New() const final {
    return CreateMaybeMessage<LaserScan>(nullptr);
  }

  LaserScan* New(::google::protobuf::Arena* arena) const final {
    return CreateMaybeMessage<LaserScan>(arena);
  }
  void CopyFrom(const ::google::protobuf::Message& from) final;
  void MergeFrom(const ::google::protobuf::Message& from) final;
  void CopyFrom(const LaserScan& from);
  void MergeFrom(const LaserScan& from);
  PROTOBUF_ATTRIBUTE_REINITIALIZES void Clear() final;
  bool IsInitialized() const final;

  size_t ByteSizeLong() const final;
  #if GOOGLE_PROTOBUF_ENABLE_EXPERIMENTAL_PARSER
  static const char* _InternalParse(const char* begin, const char* end, void* object, ::google::protobuf::internal::ParseContext* ctx);
  ::google::protobuf::internal::ParseFunc _ParseFunc() const final { return _InternalParse; }
  #else
  bool MergePartialFromCodedStream(
      ::google::protobuf::io::CodedInputStream* input) final;
  #endif  // GOOGLE_PROTOBUF_ENABLE_EXPERIMENTAL_PARSER
  void SerializeWithCachedSizes(
      ::google::protobuf::io::CodedOutputStream* output) const final;
  ::google::protobuf::uint8* InternalSerializeWithCachedSizesToArray(
      ::google::protobuf::uint8* target) const final;
  int GetCachedSize() const final { return _cached_size_.Get(); }

  private:
  void SharedCtor();
  void SharedDtor();
  void SetCachedSize(int size) const final;
  void InternalSwap(LaserScan* other);
  private:
  inline ::google::protobuf::Arena* GetArenaNoVirtual() const {
    return nullptr;
  }
  inline void* MaybeArenaPtr() const {
    return nullptr;
  }
  public:

  ::google::protobuf::Metadata GetMetadata() const final;

  // nested types ----------------------------------------------------

  // accessors -------------------------------------------------------

  // repeated float scan = 5 [packed = true];
  int scan_size() const;
  void clear_scan();
  static const int kScanFieldNumber = 5;
  float scan(int index) const;
  void set_scan(int index, float value);
  void add_scan(float value);
  const ::google::protobuf::RepeatedField< float >&
      scan() const;
  ::google::protobuf::RepeatedField< float >*
      mutable_scan();

  // string name = 1;
  void clear_name();
  static const int kNameFieldNumber = 1;
  const ::std::string& name() const;
  void set_name(const ::std::string& value);
  #if LANG_CXX11
  void set_name(::std::string&& value);
  #endif
  void set_name(const char* value);
  void set_name(const char* value, size_t size);
  ::std::string* mutable_name();
  ::std::string* release_name();
  void set_allocated_name(::std::string* name);

  // uint32 id = 2;
  void clear_id();
  static const int kIdFieldNumber = 2;
  ::google::protobuf::uint32 id() const;
  void set_id(::google::protobuf::uint32 value);

  // uint32 seq = 3;
  void clear_seq();
  static const int kSeqFieldNumber = 3;
  ::google::protobuf::uint32 seq() const;
  void set_seq(::google::protobuf::uint32 value);

  // float stamp = 4;
  void clear_stamp();
  static const int kStampFieldNumber = 4;
  float stamp() const;
  void set_stamp(float value);

  // @@protoc_insertion_point(class_scope:nus_protocol.LaserScan)
 private:
  class HasBitSetters;

  ::google::protobuf::internal::InternalMetadataWithArena _internal_metadata_;
  ::google::protobuf::RepeatedField< float > scan_;
  mutable std::atomic<int> _scan_cached_byte_size_;
  ::google::protobuf::internal::ArenaStringPtr name_;
  ::google::protobuf::uint32 id_;
  ::google::protobuf::uint32 seq_;
  float stamp_;
  mutable ::google::protobuf::internal::CachedSize _cached_size_;
  friend struct ::TableStruct_laser_5fscan_2eproto;
};
// ===================================================================


// ===================================================================

#ifdef __GNUC__
  #pragma GCC diagnostic push
  #pragma GCC diagnostic ignored "-Wstrict-aliasing"
#endif  // __GNUC__
// LaserScan

// string name = 1;
inline void LaserScan::clear_name() {
  name_.ClearToEmptyNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
inline const ::std::string& LaserScan::name() const {
  // @@protoc_insertion_point(field_get:nus_protocol.LaserScan.name)
  return name_.GetNoArena();
}
inline void LaserScan::set_name(const ::std::string& value) {
  
  name_.SetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), value);
  // @@protoc_insertion_point(field_set:nus_protocol.LaserScan.name)
}
#if LANG_CXX11
inline void LaserScan::set_name(::std::string&& value) {
  
  name_.SetNoArena(
    &::google::protobuf::internal::GetEmptyStringAlreadyInited(), ::std::move(value));
  // @@protoc_insertion_point(field_set_rvalue:nus_protocol.LaserScan.name)
}
#endif
inline void LaserScan::set_name(const char* value) {
  GOOGLE_DCHECK(value != nullptr);
  
  name_.SetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), ::std::string(value));
  // @@protoc_insertion_point(field_set_char:nus_protocol.LaserScan.name)
}
inline void LaserScan::set_name(const char* value, size_t size) {
  
  name_.SetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(),
      ::std::string(reinterpret_cast<const char*>(value), size));
  // @@protoc_insertion_point(field_set_pointer:nus_protocol.LaserScan.name)
}
inline ::std::string* LaserScan::mutable_name() {
  
  // @@protoc_insertion_point(field_mutable:nus_protocol.LaserScan.name)
  return name_.MutableNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
inline ::std::string* LaserScan::release_name() {
  // @@protoc_insertion_point(field_release:nus_protocol.LaserScan.name)
  
  return name_.ReleaseNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
inline void LaserScan::set_allocated_name(::std::string* name) {
  if (name != nullptr) {
    
  } else {
    
  }
  name_.SetAllocatedNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), name);
  // @@protoc_insertion_point(field_set_allocated:nus_protocol.LaserScan.name)
}

// uint32 id = 2;
inline void LaserScan::clear_id() {
  id_ = 0u;
}
inline ::google::protobuf::uint32 LaserScan::id() const {
  // @@protoc_insertion_point(field_get:nus_protocol.LaserScan.id)
  return id_;
}
inline void LaserScan::set_id(::google::protobuf::uint32 value) {
  
  id_ = value;
  // @@protoc_insertion_point(field_set:nus_protocol.LaserScan.id)
}

// uint32 seq = 3;
inline void LaserScan::clear_seq() {
  seq_ = 0u;
}
inline ::google::protobuf::uint32 LaserScan::seq() const {
  // @@protoc_insertion_point(field_get:nus_protocol.LaserScan.seq)
  return seq_;
}
inline void LaserScan::set_seq(::google::protobuf::uint32 value) {
  
  seq_ = value;
  // @@protoc_insertion_point(field_set:nus_protocol.LaserScan.seq)
}

// float stamp = 4;
inline void LaserScan::clear_stamp() {
  stamp_ = 0;
}
inline float LaserScan::stamp() const {
  // @@protoc_insertion_point(field_get:nus_protocol.LaserScan.stamp)
  return stamp_;
}
inline void LaserScan::set_stamp(float value) {
  
  stamp_ = value;
  // @@protoc_insertion_point(field_set:nus_protocol.LaserScan.stamp)
}

// repeated float scan = 5 [packed = true];
inline int LaserScan::scan_size() const {
  return scan_.size();
}
inline void LaserScan::clear_scan() {
  scan_.Clear();
}
inline float LaserScan::scan(int index) const {
  // @@protoc_insertion_point(field_get:nus_protocol.LaserScan.scan)
  return scan_.Get(index);
}
inline void LaserScan::set_scan(int index, float value) {
  scan_.Set(index, value);
  // @@protoc_insertion_point(field_set:nus_protocol.LaserScan.scan)
}
inline void LaserScan::add_scan(float value) {
  scan_.Add(value);
  // @@protoc_insertion_point(field_add:nus_protocol.LaserScan.scan)
}
inline const ::google::protobuf::RepeatedField< float >&
LaserScan::scan() const {
  // @@protoc_insertion_point(field_list:nus_protocol.LaserScan.scan)
  return scan_;
}
inline ::google::protobuf::RepeatedField< float >*
LaserScan::mutable_scan() {
  // @@protoc_insertion_point(field_mutable_list:nus_protocol.LaserScan.scan)
  return &scan_;
}

#ifdef __GNUC__
  #pragma GCC diagnostic pop
#endif  // __GNUC__

// @@protoc_insertion_point(namespace_scope)

}  // namespace nus_protocol

// @@protoc_insertion_point(global_scope)

#include <google/protobuf/port_undef.inc>
#endif  // PROTOBUF_INCLUDED_laser_5fscan_2eproto
