// Copyright 2015-2017 Yuchao Hu

#ifndef NUS_UNITY_SOCKET_MESSAGE_HANDLER_STEREO_H
#define NUS_UNITY_SOCKET_MESSAGE_HANDLER_STEREO_H

#include <ros/ros.h>
#include <string>
#include "nus_unity_socket/message_handler.h"
#include "nus_unity_socket/message_parser.h"
#include "protobuf/fragmentation.pb.h"
#include "protobuf/stereo.pb.h"

namespace nus_unity_socket
{
class MessageHandlerStereo : public MessageHandler
{
public:
  MessageHandlerStereo() = delete;
  MessageHandlerStereo(const MessageHandlerStereo&) = delete;
  MessageHandlerStereo& operator=(const MessageHandlerStereo&) = delete;
  explicit MessageHandlerStereo(ros::NodeHandle& node);

  void ParseChar(const uint8_t& cp);
  void ParseMessage(const void* message_ptr, std::size_t bytes_transferred);

private:
  void HandleMessage(const nus_protocol::Fragmentation& message);
  void HandleMessage(const nus_protocol::Stereo& message);

private:
  ros::Publisher sensor_stereo_left_image_raw_pub_;
  ros::Publisher sensor_stereo_left_camera_info_pub_;
  ros::Publisher sensor_stereo_right_image_raw_pub_;
  ros::Publisher sensor_stereo_right_camera_info_pub_;

  nus_protocol::Fragmentation frag_msg_;
  nus_protocol::Stereo stereo_msg_;

  std::string frag_data_;
  int frag_total_size_;
  int frag_seq_; 
  int frag_num_; 
  int frag_id_; 
};  // class MessageHandlerStereo

}  // namespace nus_unity_socket

#endif  // NUS_UNITY_SOCKET_MESSAGE_HANDLER_STEREO_H
