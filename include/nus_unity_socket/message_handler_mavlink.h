// Copyright 2015-2017 Yuchao Hu

#ifndef NUS_UNITY_SOCKET_MESSAGE_HANDLER_MAVLINK_H
#define NUS_UNITY_SOCKET_MESSAGE_HANDLER_MAVLINK_H

#include <ros/ros.h>
#include "mavlink/v1.0/common/mavlink.h"
#include "nus_unity_socket/message_handler.h"

namespace nus_unity_socket
{
class MessageHandlerMavlink : public MessageHandler
{
public:
  MessageHandlerMavlink() = delete;
  MessageHandlerMavlink(const MessageHandlerMavlink&) = delete;
  MessageHandlerMavlink& operator=(const MessageHandlerMavlink&) = delete;
  explicit MessageHandlerMavlink(ros::NodeHandle& node);

  void ParseChar(const uint8_t& cp);
  void ParseMessage(const void* message_ptr, std::size_t bytes_transferred);

private:
  void HandleMessage(const mavlink_message_t& message, uint8_t sysid, uint8_t compid);
  void HandleHilRcInputRaw(const mavlink_message_t& msg, const uint8_t& sysid, const uint8_t& compid);
  void HandleHilSensor(const mavlink_message_t& msg, const uint8_t& sysid, const uint8_t& compid);
  void HandleHilGps(const mavlink_message_t& msg, const uint8_t& sysid, const uint8_t& compid);
  void HandleHilOpticalFlow(const mavlink_message_t& msg, const uint8_t& sysid, const uint8_t& compid);
  void HandleHilStateQuaternion(const mavlink_message_t& msg, const uint8_t& sysid, const uint8_t& compid);

private:
  int channel;
  mavlink_message_t received_message;
  mavlink_status_t received_status;
};  // class MessageHandlerMavlink

}  // namespace nus_unity_socket

#endif  // NUS_UNITY_SOCKET_MESSAGE_HANDLER_MAVLINK_H
