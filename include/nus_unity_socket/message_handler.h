// Copyright 2015-2017 Yuchao Hu

#ifndef NUS_UNITY_SOCKET_MESSAGE_HANDLER_H
#define NUS_UNITY_SOCKET_MESSAGE_HANDLER_H

#include <cstdint>

namespace nus_unity_socket
{
class MessageHandler
{
public:
  virtual void ParseChar(const uint8_t& cp) = 0;
  virtual void ParseMessage(const void* message_ptr, std::size_t bytes_transferred) = 0;
};  // class MessageHandler

}  // namespace nus_unity_socket

#endif  // NUS_UNITY_SOCKET_MESSAGE_HANDLER_H
