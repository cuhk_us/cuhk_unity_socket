// Copyright 2017 Yuchao Hu

#ifndef NUS_UNITY_SOCKET_UTILS_H
#define NUS_UNITY_SOCKET_UTILS_H

#include <map>
#include <string>

namespace nus_unity_socket
{
namespace utils
{
class Transformations
{
public:
  struct Quaternion
  {
    double w = 1.0;
    double x = 0.0;
    double y = 0.0;
    double z = 0.0;
  };
  typedef std::array<std::array<double, 4>, 4> Matrix4x4;
  enum RotationConvention
  {
    kSXYZ,
    kSXYX,
    kSXZY,
    kSXZX,
    kSYZX,
    kSYZY,
    kSYXZ,
    kSYXY,
    kSZXY,
    kSZXZ,
    kSZYX,
    kSZYZ,
    kRZYX,
    kRXYX,
    kRYZX,
    kRXZX,
    kRXZY,
    kRYZY,
    kRZXY,
    kRYXY,
    kRYXZ,
    kRZXZ,
    kRXYZ,
    kRZYZ
  };
  static int eulerFromQuaternion(const Quaternion& quat, const enum RotationConvention& axes, double* roll,
                                 double* pitch, double* yaw);
  static int quaternionFromEuler(const double& roll, const double& pitch, const double& yaw,
                                 const enum RotationConvention& axes, Quaternion* quat);

private:
  static int eulerFromMatrix(const Matrix4x4& matrix, const enum RotationConvention& axes, double* roll, double* pitch,
                             double* yaw);
  static int matrixFromQuaternion(const Quaternion& quat, Matrix4x4* matrix);

private:
  static std::map<RotationConvention, std::array<int, 4>> axis_to_tuple_;
  static std::array<int, 4> next_axis_;
};  // class Transformations

}  // namespace utils
}  // namespace nus_unity_socket

#endif  // NUS_UNITY_SOCKET_UTILS_H
