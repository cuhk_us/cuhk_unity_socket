// Copyright 2015-2017 Yuchao Hu

#ifndef NUS_UNITY_SOCKET_MESSAGE_HANDLER_BASIC_H
#define NUS_UNITY_SOCKET_MESSAGE_HANDLER_BASIC_H

#include <ros/ros.h>
#include <tf/transform_broadcaster.h>
#include "nus_unity_socket/check_sum.h"
#include "nus_unity_socket/message_handler.h"
#include "nus_unity_socket/message_parser.h"
#include "protobuf/basic.pb.h"

namespace nus_unity_socket
{
class MessageHandlerBasic : public MessageHandler
{
public:
  MessageHandlerBasic() = delete;
  MessageHandlerBasic(const MessageHandlerBasic&) = delete;
  MessageHandlerBasic& operator=(const MessageHandlerBasic&) = delete;
  explicit MessageHandlerBasic(ros::NodeHandle& node);

  void ParseChar(const uint8_t& cp);
  void ParseMessage(const void* message_ptr, std::size_t bytes_transferred);

private:
  void HandleMessage(const nus_protocol::Basic& message);
  void QuaternionFromEunLeftHandedToEulerNwuRightHanded(const nus_protocol::Quaternion& t,
                                                        nus_protocol::Quaternion& o) const;

private:
  ros::Publisher sensor_laser_altitude_pub_;
  ros::Publisher sensor_imu_pub_;
  ros::Publisher sensor_magnetic_field_pub_;
  ros::Publisher sensor_absolute_pressure_pub_;
  ros::Publisher sensor_differential_pressure_pub_;
  ros::Publisher sensor_pressure_altitude_pub_;
  ros::Publisher sensor_temperature_pub_;
  ros::Publisher sensor_gps_pub_;
  ros::Publisher state_ground_truth_pub_;
  ros::Publisher state_measurement_pub_;
  ros::Publisher local_position_ned_pub_;
  ros::Publisher local_position_nwu_pub_;
  ros::Publisher human_position_pub_;

  tf::TransformBroadcaster br;

  nus_protocol::Basic basic_msg_;
};  // class MessageHandlerBasic

}  // namespace nus_unity_socket

#endif  // NUS_UNITY_SOCKET_MESSAGE_HANDLER_BASIC_H
