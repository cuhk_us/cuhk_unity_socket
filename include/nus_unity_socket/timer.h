#ifndef TIMER_H
#define TIMER_H



#include <sys/time.h>
#include <stdio.h>



struct timer{

  void start(){
    gettimeofday(&t_b, 0);
  }
  double elapsed()
  {
    gettimeofday(&t_e, 0);
    double timefly=(t_e.tv_sec - t_b.tv_sec) + (t_e.tv_usec - t_b.tv_usec)*1e-6;
    return timefly;
  }
  timeval t_b, t_e;
};
#define TIME_BEGIN() {timeval t_b, t_e; gettimeofday(&t_b, 0);
#define TIME_END(TAG) gettimeofday(&t_e, 0); printf("=== %s time: %lf s\n", TAG, (t_e.tv_sec - t_b.tv_sec) + (t_e.tv_usec - t_b.tv_usec)*1e-6); }

#endif // TIMER_H
