// Copyright 2015-2017 Yuchao Hu

#ifndef NUS_UNITY_SOCKET_CHECK_SUM_H
#define NUS_UNITY_SOCKET_CHECK_SUM_H

#include "ros/ros.h"

namespace nus_unity_socket
{
class CheckSum
{
public:
  CheckSum();

private:
  uint16_t ccitt_table[256];

public:
  uint16_t CRC16(uint8_t* buf, int index, int len);
  bool CheckCRC16(uint8_t* buf, int index, int buf_len, uint16_t crc);
};

}  // namespace nus_unity_socket

#endif  // NUS_UNITY_SOCKET_CHECK_SUM_H
