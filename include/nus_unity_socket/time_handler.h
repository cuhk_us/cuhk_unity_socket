// Copyright 2015-2017 Yuchao Hu

#ifndef NUS_UNITY_SOCKET_TIME_HANDLER_H
#define NUS_UNITY_SOCKET_TIME_HANDLER_H

#include <ros/ros.h>

namespace nus_unity_socket
{
class TimeHandler
{
public:
  static void SetInitUnityTime(float unity_time);
  static bool IsInitialized();
  static ros::Time GetTime(float unity_time);

private:
  static bool initialized;
  static float init_unity_time;
  static ros::Time init_ros_time;
};  // class TimeHandler

}  // namespace nus_unity_socket

#endif  // NUS_UNITY_SOCKET_TIME_HANDLER_H
