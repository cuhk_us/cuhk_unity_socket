// Copyright 2015-2017 Yuchao Hu

#ifndef NUS_UNITY_SOCKET_MESSAGE_HANDLER_LASER3D_H
#define NUS_UNITY_SOCKET_MESSAGE_HANDLER_LASER3D_H

#include <pcl/point_types.h>
#include <pcl_ros/point_cloud.h>
#include <ros/ros.h>
#include "nus_unity_socket/check_sum.h"
#include "nus_unity_socket/message_handler.h"
#include "nus_unity_socket/message_parser.h"
#include "protobuf/laser_scan.pb.h"

namespace nus_unity_socket
{
class MessageHandlerLaser3D : public MessageHandler
{
private:
  typedef pcl::PointXYZ PointT;
  typedef pcl::PointCloud<PointT> PointCloudT;

public:
  MessageHandlerLaser3D() = delete;
  MessageHandlerLaser3D(const MessageHandlerLaser3D&) = delete;
  MessageHandlerLaser3D& operator=(const MessageHandlerLaser3D&) = delete;
  explicit MessageHandlerLaser3D(ros::NodeHandle& node);

  void ParseChar(const uint8_t& cp);
  void ParseMessage(const void* message_ptr, std::size_t bytes_transferred);

private:
  void HandleMessage(const nus_protocol::LaserScan& message);
  void FillPointCloud(const nus_protocol::LaserScan& message, const PointCloudT::Ptr& pcl_cloud_ptr, int index);

private:
  ros::Publisher sensor_laser3d_pointcloud_pub_;
  nus_protocol::LaserScan laser_scan_msg_;
};  // class MessageHandlerLaser3D

}  // namespace nus_unity_socket

#endif  // NUS_UNITY_SOCKET_MESSAGE_HANDLER_LASER3D_H
