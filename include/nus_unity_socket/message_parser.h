// Copyright 2015-2017 Yuchao Hu

#ifndef NUS_UNITY_SOCKET_MESSAGE_PARSER_H
#define NUS_UNITY_SOCKET_MESSAGE_PARSER_H

#include <arpa/inet.h>
#include <cstdint>

namespace nus_unity_socket
{
template <typename MessageType>
class MessageParser
{
private:
  enum ParserStatus
  {
    kParserStatusUninit,
    kParserStatusGettingPrefix,
    kParserStatusGettingSize,
    kParserStatusGettingData,
    kParserStatusGotFinalData
  };

public:
  MessageParser();
  MessageParser(const MessageParser&) = delete;
  MessageParser& operator=(const MessageParser&) = delete;

  bool ParseChar(const uint8_t& cp);
  const MessageType& GetMessage();

private:
  uint8_t prefix[4];
  ParserStatus parser_status_;
  int parser_index_;
  int32_t parser_data_size_;
  MessageType message_;
};  // class MessageParser

template <typename MessageType>
MessageParser<MessageType>::MessageParser()
{
  prefix[0] = 0xfe;
  prefix[1] = 0xef;
  prefix[2] = 0xfd;
  prefix[3] = 0xef;
  parser_status_ = kParserStatusUninit;
  parser_index_ = 0;
  parser_data_size_ = 0;
}

template <typename MessageType>
bool MessageParser<MessageType>::ParseChar(const uint8_t& cp)
{
  bool rt = false;
  switch (parser_status_)
  {
    case kParserStatusGotFinalData:
    case kParserStatusUninit:
    {
      parser_index_ = 0;
      // Note: here no break
    }
    case kParserStatusGettingPrefix:
    {
      if (cp == prefix[parser_index_])
      {
        parser_status_ = kParserStatusGettingPrefix;
        ++parser_index_;
        if (parser_index_ >= sizeof(prefix))
        {
          parser_status_ = kParserStatusGettingSize;
          parser_index_ = 0;
        }
      }
      else
      {
        parser_status_ = kParserStatusUninit;
      }
      break;
    }
    case kParserStatusGettingSize:
    {
      *(reinterpret_cast<char*>(&parser_data_size_) + parser_index_) = cp;
      *(reinterpret_cast<char*>(&message_) + parser_index_) = cp;
      ++parser_index_;
      if (parser_index_ >= sizeof(parser_data_size_))
      {
        parser_data_size_ = ntohl(parser_data_size_);
        if (parser_data_size_ > sizeof(MessageType) - sizeof(parser_data_size_))
        {
          parser_status_ = kParserStatusUninit;
        }
        else
        {
          parser_status_ = kParserStatusGettingData;
          parser_index_ = 0;
        }
      }
      break;
    }
    case kParserStatusGettingData:
    {
      *(reinterpret_cast<char*>(&message_) + sizeof(parser_data_size_) + parser_index_) = cp;
      ++parser_index_;
      if (parser_index_ >= parser_data_size_)
      {
        parser_status_ = kParserStatusGotFinalData;
        rt = true;
      }
      break;
    }
  }
  return rt;
}

template <typename MessageType>
const MessageType& MessageParser<MessageType>::GetMessage()
{
  return message_;
}

}  // namespace nus_unity_socket

#endif  // NUS_UNITY_SOCKET_MESSAGE_PARSER_H
