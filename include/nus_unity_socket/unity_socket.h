// Copyright 2015-2017 Yuchao Hu

#ifndef NUS_UNITY_SOCKET_UNITY_SOCKET_H
#define NUS_UNITY_SOCKET_UNITY_SOCKET_H

#include <mavlink/v1.0/common/mavlink.h>
#include <ros/ros.h>
#include <tf/transform_broadcaster.h>
#include <string>
#include "mavros/Mavlink.h"
#include "nus_unity_socket/check_sum.h"
#include "nus_unity_socket/udp_client.h"

namespace nus_unity_socket
{
#pragma pack(push)
#pragma pack(1)
struct hil_controls_t
{
  uint32_t prefix;
  int32_t size;
  int32_t id;
  uint64_t time_usec;
  float roll_ailerons;
  float pitch_elevator;
  float yaw_rudder;
  float throttle;
  float aux1;
  float aux2;
  float aux3;
  float aux4;
  uint8_t mode;
  uint8_t nav_mode;
  uint16_t crc;  // from size to nav_mode
};
#pragma pack(pop)

class UnitySocket
{
private:
  ros::NodeHandle node_;

  ros::Subscriber mavlink_sub_;

  int server_port_;
  std::string p_server_ip_;
  int p_server_port_basic_;
  int p_server_port_image_;
  int p_server_port_stereo_;
  int p_server_port_laser2d_;
  int p_server_port_laser3d_;
  int p_server_port_mavlink_;

  std::shared_ptr<UdpClient> udp_client_basic_ptr_;
  std::shared_ptr<UdpClient> udp_client_laser2d_ptr_;
  std::shared_ptr<UdpClient> udp_client_laser3d_ptr_;
  std::shared_ptr<UdpClient> udp_client_image_ptr_;
  std::shared_ptr<UdpClient> udp_client_stereo_ptr_;
  std::shared_ptr<UdpClient> udp_client_mavlink_ptr_;

  CheckSum check_sum_;

private:
  void MavlinkCallback(const mavros::Mavlink::ConstPtr& msg_ptr);

public:
  UnitySocket();
  ~UnitySocket();
};  // class UnitySocket

}  // namespace nus_unity_socket

#endif  // NUS_UNITY_SOCKET_UNITY_SOCKET_H
