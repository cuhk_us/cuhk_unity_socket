// Copyright 2015-2017 Yuchao Hu

#ifndef NUS_UNITY_SOCKET_UDP_CLIENT_H
#define NUS_UNITY_SOCKET_UDP_CLIENT_H

#include <boost/asio.hpp>
#include <boost/thread.hpp>
#include <functional>
#include <memory>
#include <string>
#include "nus_unity_socket/message_handler.h"

namespace nus_unity_socket
{
class UdpClient : public std::enable_shared_from_this<UdpClient>
{
public:
  UdpClient(const std::string& remote_ip, const int& remote_port,
            const std::shared_ptr<MessageHandler>& message_handler_ptr);
  ~UdpClient();

  void Connect();
  void Disconnect();
  void Send(const void* bufs, const size_t& count);

private:
  void Init(const std::string& ip, const int& port);
  void Receive();
  void ReadHandler(const boost::system::error_code& error, std::size_t bytes_transferred);
  void RunThread();
  void Close();

private:
  boost::asio::io_service io_service_;
  std::unique_ptr<boost::asio::io_service::work> io_work_ptr_;
  boost::thread io_thread_;
  boost::asio::ip::udp::socket udp_socket_;
  boost::asio::ip::udp::endpoint udp_remote_endpoint_;
  boost::asio::ip::udp::endpoint udp_bind_endpoint_;
  std::array<uint8_t, 65535> rx_buffer_;

  std::shared_ptr<MessageHandler> message_handler_ptr_;
};  // class UdpClient

}  // namespace nus_unity_socket

#endif  // NUS_UNITY_SOCKET_UDP_CLIENT_H
