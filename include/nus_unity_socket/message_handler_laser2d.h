// Copyright 2015-2017 Yuchao Hu

#ifndef NUS_UNITY_SOCKET_MESSAGE_HANDLER_LASER2D_H
#define NUS_UNITY_SOCKET_MESSAGE_HANDLER_LASER2D_H

#include <ros/ros.h>
#include "nus_unity_socket/check_sum.h"
#include "nus_unity_socket/message_handler.h"
#include "nus_unity_socket/message_parser.h"
#include "protobuf/laser_scan.pb.h"

namespace nus_unity_socket
{
class MessageHandlerLaser2D : public MessageHandler
{
public:
  MessageHandlerLaser2D() = delete;
  MessageHandlerLaser2D(const MessageHandlerLaser2D&) = delete;
  MessageHandlerLaser2D& operator=(const MessageHandlerLaser2D&) = delete;
  explicit MessageHandlerLaser2D(ros::NodeHandle& node);

  void ParseChar(const uint8_t& cp);
  void ParseMessage(const void* message_ptr, std::size_t bytes_transferred);

private:
  void HandleMessage(const nus_protocol::LaserScan& message);

private:
  ros::Publisher sensor_laser_scan_pub_;
  nus_protocol::LaserScan laser_scan_msg_;
};  // class MessageHandlerLaser2D

}  // namespace nus_unity_socket

#endif  // NUS_UNITY_SOCKET_MESSAGE_HANDLER_LASER2D_H
