// Copyright 2015-2017 Yuchao Hu

#ifndef NUS_UNITY_SOCKET_MESSAGE_HANDLER_IMAGE_H
#define NUS_UNITY_SOCKET_MESSAGE_HANDLER_IMAGE_H

#include <ros/ros.h>
#include <string>
#include "nus_unity_socket/message_handler.h"
#include "nus_unity_socket/message_parser.h"
#include "protobuf/fragmentation.pb.h"
#include "protobuf/image.pb.h"

namespace nus_unity_socket
{
class MessageHandlerImage : public MessageHandler
{
public:
  MessageHandlerImage() = delete;
  MessageHandlerImage(const MessageHandlerImage&) = delete;
  MessageHandlerImage& operator=(const MessageHandlerImage&) = delete;
  explicit MessageHandlerImage(ros::NodeHandle& node);

  void ParseChar(const uint8_t& cp);
  void ParseMessage(const void* message_ptr, std::size_t bytes_transferred);

private:
  void HandleMessage(const nus_protocol::Fragmentation& message);
  void HandleMessage(const nus_protocol::Image& message);

private:
  ros::Publisher sensor_image_pub_;

  nus_protocol::Fragmentation frag_msg_;
  nus_protocol::Image image_msg_;

  std::string frag_data_;
  int frag_total_size_ = 0;
  int frag_seq_ = 0;
  int frag_num_ = 0;
  int frag_id_ = -1;
};  // class MessageHandlerImage

}  // namespace nus_unity_socket

#endif  // NUS_UNITY_SOCKET_MESSAGE_HANDLER_IMAGE_H
