# Overview

## What does this software do?
Communicate between ROS and Unity simulator.

## Specifications

## Limitations
Only support UDP protocol.

# ROS API

## nus\_unity\_socket

### Subscribed Topics
- */mavlink/from_pixhawk (mavros::Mavlink)*
MAVLink messages, which will be send to simulator.

### Published Topics
- */hil/sensor/laser_altitude (sensor_msgs::Range)*
Altitude data measured by laser from simulator.
- */hil/sensor/imu (sensor_msgs::Imu)*
IMU data from simulator.
- */hil/sensor/magnetic_field (sensor_msgs::MagneticField)*
Magnetic field data from simulator.
- */hil/sensor/absolute_pressure (sensor_msgs::FluidPressure)*
Absolute pressure data from simulator.
- */hil/sensor/differential_pressure (sensor_msgs::FluidPressure)*
Differential pressue data from simulator.
- */hil/sensor/pressure_altitude (sensor_msgs::Range)*
Altitude data measured by pressure from simulator.
- */hil/sensor/temperature (sensor_msgs::Temperature)*
Temperature data from simulator.
- */hil/sensor/gps (sensor_msgs::NavSatFix)*
GPS data from simulator.
- */hil/state/ground_truth (nus_msgs::StateWithCovarianceStamped)*
Ground truth data from simulator.
- */hil/state/measurement (nus_msgs::StateWithCovarianceStamped)*
Measurement data from simulator.

### Services

### Parameters
- *~server_ip* (string, default: 172.16.142.34)
Unity simulator IP address.
- *~server_port_basic* (int, default: 26000)
Unity simulator port for basic data.
- *~server_port_image* (int, default: 26500)
Unity simulator port for image data.
- *~server_port_stereo* (int, default: 26510)
Unity simulator port for stereo image data.
- *~server_port_laser2d* (int, default: 26600)
Unity simulator port for 2D laser data.
- *~server_port_laser3d* (int, default: 26700)
Unity simulator port for 3D laser data.
- *~server_port_mavlink* (int, default: 27000)
Unity simulator port for MAVLink data.
