// Copyright 2015-2017 Yuchao Hu

#include "nus_unity_socket/message_handler_basic.h"
#include <arpa/inet.h>
#include <sensor_msgs/FluidPressure.h>
#include <sensor_msgs/Imu.h>
#include <sensor_msgs/MagneticField.h>
#include <sensor_msgs/NavSatFix.h>
#include <sensor_msgs/Range.h>
#include <sensor_msgs/Temperature.h>
#include "common/EarthUtils.hpp"
#include "nus_msgs/StateWithCovarianceStamped.h"
#include "nus_unity_socket/time_handler.h"

namespace nus_unity_socket
{
MessageHandlerBasic::MessageHandlerBasic(ros::NodeHandle& node)
{
  // sensor
  sensor_laser_altitude_pub_ = node.advertise<sensor_msgs::Range>("/hil/sensor/laser_altitude", 10);
  sensor_imu_pub_ = node.advertise<sensor_msgs::Imu>("/hil/sensor/imu", 10);
  sensor_magnetic_field_pub_ = node.advertise<sensor_msgs::MagneticField>("/hil/sensor/magnetic_field", 10);
  sensor_absolute_pressure_pub_ = node.advertise<sensor_msgs::FluidPressure>("/hil/sensor/absolute_pressure", 10);
  sensor_differential_pressure_pub_ = node.advertise<sensor_msgs::FluidPressure>("/hil/sensor/differential_pressure", 10);
  sensor_pressure_altitude_pub_ = node.advertise<sensor_msgs::Range>("/hil/sensor/pressure_altitude", 10);
  sensor_temperature_pub_ = node.advertise<sensor_msgs::Temperature>("/hil/sensor/temperature", 10);
  sensor_gps_pub_ = node.advertise<sensor_msgs::NavSatFix>("/hil/sensor/gps", 10);
  // state
  state_ground_truth_pub_ = node.advertise<nus_msgs::StateWithCovarianceStamped>("/hil/state/ground_truth", 10);
  state_measurement_pub_ = node.advertise<nus_msgs::StateWithCovarianceStamped>("/hil/state/measurement", 10);
  local_position_ned_pub_ = node.advertise<geometry_msgs::PoseStamped>("/hil/position/local_ned/sys_id_1", 10);
  local_position_nwu_pub_ = node.advertise<geometry_msgs::PoseStamped>("/hil/position/local_nwu/sys_id_1", 10);
  // 
  human_position_pub_ = node.advertise<geometry_msgs::Point>("hil/position/human", 10);
}

void MessageHandlerBasic::ParseChar(const uint8_t& cp)
{
}

void MessageHandlerBasic::ParseMessage(const void* message_ptr, std::size_t bytes_transferred)
{
  if (!basic_msg_.ParseFromArray(message_ptr, bytes_transferred))
  {
    return;
  }
  HandleMessage(basic_msg_);
}

void MessageHandlerBasic::HandleMessage(const nus_protocol::Basic& message)
{
  if (!TimeHandler::IsInitialized())
  {
    TimeHandler::SetInitUnityTime(message.stamp());
    std::cout << "SetInitUnityTime to " << message.stamp() << std::endl; // FIXME: always return zero
  }
  // FIXME: ros::Time time_now = TimeHandler::GetTime(message.stamp());
  // use Time::now() instead of message.stamp() which always return zero 
  ros::Time time_now = ros::Time::now();
  {
    // publish sensor laser altitude message
    if (sensor_laser_altitude_pub_.getNumSubscribers() > 0)
    {
      sensor_msgs::Range sensor_laser_altitude_msg;
      sensor_laser_altitude_msg.header.stamp = time_now;
      sensor_laser_altitude_msg.header.frame_id = "";
      sensor_laser_altitude_msg.range = message.laser_altitude();
      sensor_laser_altitude_pub_.publish(sensor_laser_altitude_msg);
    }

    // publish measurement tf
    tf::Transform transform;
    tf::Quaternion q;
    transform.setOrigin(tf::Vector3(message.measurement().position().z(), -message.measurement().position().x(),
                                    message.measurement().position().y()));
    nus_protocol::Quaternion orientation;
    QuaternionFromEunLeftHandedToEulerNwuRightHanded(message.measurement().orientation(), orientation);
    q = tf::Quaternion(orientation.x(), orientation.y(), orientation.z(), orientation.w());
    transform.setRotation(q);
    br.sendTransform(tf::StampedTransform(transform, time_now, "/map", "/hil/state/measurement"));

    // publish measurement message
    if (state_measurement_pub_.getNumSubscribers() > 0)
    {
      nus_msgs::StateWithCovarianceStamped state_measurement_msg;
      state_measurement_msg.header.stamp = time_now;
      state_measurement_msg.header.frame_id = "";  // FIXME
      state_measurement_msg.pose.pose.position.x = message.measurement().position().z();
      state_measurement_msg.pose.pose.position.y = -message.measurement().position().x();
      state_measurement_msg.pose.pose.position.z = message.measurement().position().y();
      tf::quaternionTFToMsg(q, state_measurement_msg.pose.pose.orientation);
      state_measurement_msg.velocity.twist.linear.x = message.measurement().velocity().z();
      state_measurement_msg.velocity.twist.linear.y = -message.measurement().velocity().x();
      state_measurement_msg.velocity.twist.linear.z = message.measurement().velocity().y();
      state_measurement_msg.velocity.twist.angular.x = -message.measurement().angular_velocity().z();  // body frame
      state_measurement_msg.velocity.twist.angular.y = message.measurement().angular_velocity().x();
      state_measurement_msg.velocity.twist.angular.z = -message.measurement().angular_velocity().y();
      state_measurement_msg.acceleration.accel.linear.x = message.measurement().acceleration().z();  // global frame
      state_measurement_msg.acceleration.accel.linear.y = -message.measurement().acceleration().x();
      state_measurement_msg.acceleration.accel.linear.z = message.measurement().acceleration().y();
      // msg.acceleration.accel.angular.x = message.d2phi;
      // msg.acceleration.accel.angular.y = message.d2tht;
      // FIXME maybe no need angular accel
      // msg.acceleration.accel.angular.z = message.d2psi;

      state_measurement_pub_.publish(state_measurement_msg);
    }

    // publish local_position_nwu_pub_ message
    if (local_position_nwu_pub_.getNumSubscribers() > 0)
    {
      geometry_msgs::PoseStamped local_position_nwu_msg;
      local_position_nwu_msg.header.stamp = time_now;
      local_position_nwu_msg.header.frame_id = "";  // FIXME
      local_position_nwu_msg.pose.position.x = message.measurement().position().z();
      local_position_nwu_msg.pose.position.y = -message.measurement().position().x();
      local_position_nwu_msg.pose.position.z = message.measurement().position().y();
      tf::quaternionTFToMsg(q, local_position_nwu_msg.pose.orientation);
      local_position_nwu_pub_.publish(local_position_nwu_msg);
    }

    // publish local_position_ned_pub_ message
    if (local_position_ned_pub_.getNumSubscribers() > 0)
    {
      geometry_msgs::PoseStamped local_position_ned_msg;
      local_position_ned_msg.header.stamp = time_now;
      local_position_ned_msg.header.frame_id = "";  // FIXME
      local_position_ned_msg.pose.position.x = message.measurement().position().z();
      local_position_ned_msg.pose.position.y = message.measurement().position().x();
      local_position_ned_msg.pose.position.z = -message.measurement().position().y();
      tf::quaternionTFToMsg(q, local_position_ned_msg.pose.orientation);  //FIXME: same as NED or not?
      local_position_ned_pub_.publish(local_position_ned_msg);
    }

    // publish IMU message
    if (sensor_imu_pub_.getNumSubscribers() > 0)
    {
      sensor_msgs::Imu sensor_imu_msg;
      sensor_imu_msg.header.stamp = time_now;
      sensor_imu_msg.header.frame_id = "/sensor/imu";
      tf::quaternionTFToMsg(q, sensor_imu_msg.orientation);
      sensor_imu_msg.orientation_covariance[0] = -1;
      sensor_imu_msg.angular_velocity.x = -message.measurement().angular_velocity().z();  // body frame
      sensor_imu_msg.angular_velocity.y = message.measurement().angular_velocity().x();
      sensor_imu_msg.angular_velocity.z = -message.measurement().angular_velocity().y();
      sensor_imu_msg.angular_velocity_covariance[0] = -1;
      msr::airlib::Vector3r linear_acceleration(message.measurement().acceleration().z(),
                                                -message.measurement().acceleration().x(),
                                                message.measurement().acceleration().y() + 9.781);
      msr::airlib::Quaternionr orientation(q.w(), q.x(), q.y(), q.z());
      msr::airlib::Vector3r linear_acceleration_body =
          msr::airlib::VectorMath::transformToBodyFrame(linear_acceleration, orientation, false);
      sensor_imu_msg.linear_acceleration.x = linear_acceleration_body.x();
      sensor_imu_msg.linear_acceleration.y = linear_acceleration_body.y();
      sensor_imu_msg.linear_acceleration.z = linear_acceleration_body.z();
      sensor_imu_msg.linear_acceleration_covariance[0] = -1;
      sensor_imu_pub_.publish(sensor_imu_msg);
    }

    if (sensor_magnetic_field_pub_.getNumSubscribers() > 0)
    {
      sensor_msgs::MagneticField sensor_magnetic_field_msg;
      sensor_magnetic_field_msg.header.stamp = time_now;
      sensor_magnetic_field_msg.header.frame_id = "/sensor/magnetometer";

      msr::airlib::Vector3r magnetic_field_true = msr::airlib::EarthUtils::getMagField(
          msr::airlib::GeoPoint(message.ground_truth().gps().lat(), message.ground_truth().gps().lon(),
                                message.ground_truth().gps().alt()));  // Tesla

      magnetic_field_true.y() = -magnetic_field_true.y();  // NED to NWU
      magnetic_field_true.z() = -magnetic_field_true.z();
      msr::airlib::Quaternionr body_orientation(q.w(), q.x(), q.y(), q.z());
      msr::airlib::Vector3r magnetic_field_body =
          msr::airlib::VectorMath::transformToBodyFrame(magnetic_field_true, body_orientation, false);
      sensor_magnetic_field_msg.magnetic_field.x = magnetic_field_body.x();
      sensor_magnetic_field_msg.magnetic_field.y = magnetic_field_body.y();
      sensor_magnetic_field_msg.magnetic_field.z = magnetic_field_body.z();
      sensor_magnetic_field_pub_.publish(sensor_magnetic_field_msg);
    }

    if (sensor_absolute_pressure_pub_.getNumSubscribers() > 0)
    {
      sensor_msgs::FluidPressure sensor_absolute_pressure_msg;
      sensor_absolute_pressure_msg.header.stamp = time_now;
      sensor_absolute_pressure_msg.header.frame_id = "/sensor/barometer";
      auto pressure = msr::airlib::EarthUtils::getStandardPressure(message.measurement().gps().alt());
      sensor_absolute_pressure_msg.fluid_pressure = pressure;
      sensor_absolute_pressure_pub_.publish(sensor_absolute_pressure_msg);
    }

    if (sensor_differential_pressure_pub_.getNumSubscribers() > 0)
    {
      sensor_msgs::FluidPressure sensor_differential_pressure_msg;
      sensor_differential_pressure_msg.header.stamp = time_now;
      sensor_differential_pressure_msg.header.frame_id = "/sensor/barometer";
      // FIXME
      sensor_differential_pressure_msg.fluid_pressure =
          std::sqrt(message.measurement().velocity().x() * message.measurement().velocity().x() +
                    message.measurement().velocity().y() * message.measurement().velocity().y() +
                    message.measurement().velocity().z() * message.measurement().velocity().z()) /
          6 * 0.02 * 1000;
      sensor_differential_pressure_pub_.publish(sensor_differential_pressure_msg);
    }

    if (sensor_pressure_altitude_pub_.getNumSubscribers() > 0)
    {
      sensor_msgs::Range sensor_pressure_altitude_msg;
      sensor_pressure_altitude_msg.header.stamp = time_now;
      sensor_pressure_altitude_msg.header.frame_id = "/sensor/barometer";
      sensor_pressure_altitude_msg.range = message.measurement().position().y();
      sensor_pressure_altitude_pub_.publish(sensor_pressure_altitude_msg);
    }

    if (sensor_temperature_pub_.getNumSubscribers() > 0)
    {
      sensor_msgs::Temperature sensor_temperature_msg;
      sensor_temperature_msg.header.stamp = time_now;
      sensor_temperature_msg.header.frame_id = "/sensor/temperature";
      sensor_temperature_msg.temperature = 20;
      sensor_temperature_pub_.publish(sensor_temperature_msg);
    }

    if (sensor_gps_pub_.getNumSubscribers() > 0)
    {
      sensor_msgs::NavSatFix sensor_gps_msg;
      sensor_gps_msg.header.stamp = time_now;
      sensor_gps_msg.header.frame_id = "";  // FIXME
      sensor_gps_msg.status.status = sensor_msgs::NavSatStatus::STATUS_GBAS_FIX;
      sensor_gps_msg.status.service = sensor_msgs::NavSatStatus::SERVICE_GPS;
      sensor_gps_msg.latitude = message.measurement().gps().lat();
      sensor_gps_msg.longitude = message.measurement().gps().lon();
      sensor_gps_msg.altitude = message.measurement().gps().alt();
      // sensor_gps_msg.altitude +=
      // mavutils::geoid_to_ellipsoid_height(&sensor_gps_msg);
      // // AMSL to WGS 84 ellipsoid
      sensor_gps_msg.position_covariance_type = sensor_msgs::NavSatFix::COVARIANCE_TYPE_UNKNOWN;
      sensor_gps_pub_.publish(sensor_gps_msg);
    }
  }

  {
    /* publish ground truth tf */
    tf::Transform transform;
    tf::Quaternion q;
    transform.setOrigin(tf::Vector3(message.ground_truth().position().z(), -message.ground_truth().position().x(),
                                    message.ground_truth().position().y()));
    nus_protocol::Quaternion orientation;
    QuaternionFromEunLeftHandedToEulerNwuRightHanded(message.ground_truth().orientation(), orientation);
    q = tf::Quaternion(orientation.x(), orientation.y(), orientation.z(), orientation.w());
    transform.setRotation(q);
    br.sendTransform(tf::StampedTransform(transform, time_now, "/map", "/hil/state/ground_truth"));

    // ground truth
    if (state_ground_truth_pub_.getNumSubscribers() > 0)
    {
      nus_msgs::StateWithCovarianceStamped msg;
      msg.header.stamp = time_now;
      msg.header.frame_id = "";  // FIXME
      msg.pose.pose.position.x = message.ground_truth().position().z();
      msg.pose.pose.position.y = -message.ground_truth().position().x();
      msg.pose.pose.position.z = message.ground_truth().position().y();
      tf::quaternionTFToMsg(q, msg.pose.pose.orientation);
      msg.velocity.twist.linear.x = message.ground_truth().velocity().z();
      msg.velocity.twist.linear.y = -message.ground_truth().velocity().x();
      msg.velocity.twist.linear.z = message.ground_truth().velocity().y();
      msg.velocity.twist.angular.x = -message.ground_truth().angular_velocity().z();  // body frame
      msg.velocity.twist.angular.y = message.ground_truth().angular_velocity().x();
      msg.velocity.twist.angular.z = -message.ground_truth().angular_velocity().y();
      msr::airlib::Vector3r linear_acceleration(message.measurement().acceleration().z(),
                                                -message.measurement().acceleration().x(),
                                                message.measurement().acceleration().y() + 9.781);
      msr::airlib::Quaternionr orientation(q.w(), q.x(), q.y(), q.z());
      msr::airlib::Vector3r linear_acceleration_body =
          msr::airlib::VectorMath::transformToBodyFrame(linear_acceleration, orientation, false);
      msg.acceleration.accel.linear.x = linear_acceleration_body.x();
      msg.acceleration.accel.linear.y = linear_acceleration_body.y();
      msg.acceleration.accel.linear.z = linear_acceleration_body.z();
      // msg.acceleration.accel.angular.x = message.d2phi;
      // msg.acceleration.accel.angular.y = message.d2tht;
      // FIXME maybe no need angular accel
      // msg.acceleration.accel.angular.z = message.d2psi;

      state_ground_truth_pub_.publish(msg);
    }
  }

  if (human_position_pub_.getNumSubscribers() > 0)
  {
    geometry_msgs::Point human_position_msg;
    human_position_msg.x = message.human_position().x();
    human_position_msg.y = message.human_position().y();
    human_position_msg.z = message.human_position().z();
    human_position_pub_.publish(human_position_msg);
  }
}

void MessageHandlerBasic::QuaternionFromEunLeftHandedToEulerNwuRightHanded(const nus_protocol::Quaternion& t,
                                                                           nus_protocol::Quaternion& o) const
{
  tf::Quaternion q(t.x(), t.y(), t.z(), t.w());
  double angle = q.getAngle();
  tf::Vector3 axis = q.getAxis();
  tf::Quaternion q_r(tf::Vector3(axis.getZ(), -axis.getX(), axis.getY()), -angle);
  o.set_x(q_r.x());
  o.set_y(q_r.y());
  o.set_z(q_r.z());
  o.set_w(q_r.w());
}

}  // namespace nus_unity_socket
