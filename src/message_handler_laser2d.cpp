// Copyright 2015-2017 Yuchao Hu

#include "nus_unity_socket/message_handler_laser2d.h"
#include <arpa/inet.h>
#include <sensor_msgs/LaserScan.h>
#include "nus_unity_socket/time_handler.h"

namespace nus_unity_socket
{
MessageHandlerLaser2D::MessageHandlerLaser2D(ros::NodeHandle& node)
{
  // sensor measurement
  sensor_laser_scan_pub_ = node.advertise<sensor_msgs::LaserScan>("/hil/sensor/laser_scan", 10);
}

void MessageHandlerLaser2D::ParseChar(const uint8_t& cp)
{
}

void MessageHandlerLaser2D::ParseMessage(const void* message_ptr, std::size_t bytes_transferred)
{
  if (!laser_scan_msg_.ParseFromArray(message_ptr, bytes_transferred))
  {
    return;
  }
  HandleMessage(laser_scan_msg_);
}

void MessageHandlerLaser2D::HandleMessage(const nus_protocol::LaserScan& message)
{
  if (sensor_laser_scan_pub_.getNumSubscribers() > 0)
  {
    ros::Time time_now = TimeHandler::GetTime(message.stamp());
    sensor_msgs::LaserScan sensor_laser_scan_msg;
    sensor_laser_scan_msg.header.stamp = time_now;
    sensor_laser_scan_msg.angle_min = -3.1415926 / 180 * 135;
    sensor_laser_scan_msg.angle_max = 3.1415926 / 180 * 135;
    sensor_laser_scan_msg.angle_increment = 3.1415926 / 180 * 270 / 1080;
    sensor_laser_scan_msg.range_min = 0.5;
    sensor_laser_scan_msg.range_max = 25;
    sensor_laser_scan_msg.header.frame_id = "/sensor/laser";
    sensor_laser_scan_msg.ranges.resize(message.scan_size());
    for (int i = 0; i < message.scan_size(); ++i)
    {
      sensor_laser_scan_msg.ranges[i] = message.scan(i);
    }
    sensor_laser_scan_pub_.publish(sensor_laser_scan_msg);
  }

/* publish laser vertical tf */
// transform.setOrigin(tf::Vector3(0, 0, 0));
// q.setRPY(message.laserVerticalRotation.x, message.laserVerticalRotation.y,
// message.laserVerticalRotation.z);
// ROS_INFO("x = %f, y = %f, z = %f", message.laserVerticalRotation.x,
// message.laserVerticalRotation.y, message.laserVerticalRotation.z);
// transform.setRotation(q);
// br.sendTransform(tf::StampedTransform(transform, time_now, "/uav",
// "/laser/vertical"));

/* publish laser horizontal tf */
#if 0
                        transform.setOrigin(tf::Vector3(0, 0, 0));
                        q.setRPY(0, 0, 0);
                        transform.setRotation(q);
                        br.sendTransform(tf::StampedTransform(transform, time_now, "/uav", "/laser/horizontal"));
#endif
}

}  // namespace nus_unity_socket
