// Copyright 2017 Yuchao Hu

#include "nus_unity_socket/utils.h"
#include <cmath>
#include <limits>
#include <map>
#include <string>

namespace nus_unity_socket
{
namespace utils
{
std::map<Transformations::RotationConvention, std::array<int, 4>> Transformations::axis_to_tuple_ = {
  { kSXYZ, { 0, 0, 0, 0 } }, { kSXYX, { 0, 0, 1, 0 } }, { kSXZY, { 0, 1, 0, 0 } }, { kSXZX, { 0, 1, 1, 0 } },
  { kSYZX, { 1, 0, 0, 0 } }, { kSYZY, { 1, 0, 1, 0 } }, { kSYXZ, { 1, 1, 0, 0 } }, { kSYXY, { 1, 1, 1, 0 } },
  { kSZXY, { 2, 0, 0, 0 } }, { kSZXZ, { 2, 0, 1, 0 } }, { kSZYX, { 2, 1, 0, 0 } }, { kSZYZ, { 2, 1, 1, 0 } },
  { kRZYX, { 0, 0, 0, 1 } }, { kRXYX, { 0, 0, 1, 1 } }, { kRYZX, { 0, 1, 0, 1 } }, { kRXZX, { 0, 1, 1, 1 } },
  { kRXZY, { 1, 0, 0, 1 } }, { kRYZY, { 1, 0, 1, 1 } }, { kRZXY, { 1, 1, 0, 1 } }, { kRYXY, { 1, 1, 1, 1 } },
  { kRYXZ, { 2, 0, 0, 1 } }, { kRZXZ, { 2, 0, 1, 1 } }, { kRXYZ, { 2, 1, 0, 1 } }, { kRZYZ, { 2, 1, 1, 1 } }
};
std::array<int, 4> Transformations::next_axis_ = { 1, 2, 0, 1 };

int Transformations::quaternionFromEuler(const double& roll, const double& pitch, const double& yaw,
                                         const enum RotationConvention& axes, Quaternion* quat)
{
  if (!quat)
    return -1;

  std::array<int, 4> tuple;
  try
  {
    tuple = axis_to_tuple_.at(axes);
  }
  catch (const std::exception& e)
  {
    return -1;
  }

  // first character : rotations are applied to 's'tatic or 'r'otating frame remaining characters : successive rotation
  // axis 'x', 'y', or 'z'

  // inner axis: code of axis ('x':0, 'y':1, 'z':2) of rightmost matrix.
  int firstAxis = tuple.at(0);
  // parity : even (0) if inner axis 'x' is followed by 'y', 'y' is followed by 'z', or 'z' is followed by 'x'.
  // Otherwise odd (1).
  int parity = tuple.at(1);
  // repetition : first and last axis are same (1) or different (0).
  int repetition = tuple.at(2);
  // frame : rotations are applied to static (0) or rotating (1) frame.
  int frame = tuple.at(3);

  int i = firstAxis + 1;
  int j = next_axis_[i + parity - 1] + 1;
  int k = next_axis_[i - parity] + 1;
  double ai = roll;
  double aj = pitch;
  double ak = yaw;
  double t;
  double si, sj, sk, ci, cj, ck, cc, cs, sc, ss;

  if (frame)
  {
    t = ai;
    ai = ak;
    ak = t;
  }

  if (parity)
  {
    aj = -aj;
  }

  ai /= 2.0;
  aj /= 2.0;
  ak /= 2.0;

  si = std::sin(ai);
  sj = std::sin(aj);
  sk = std::sin(ak);
  ci = std::cos(ai);
  cj = std::cos(aj);
  ck = std::cos(ak);
  cc = ci * ck;
  cs = ci * sk;
  sc = si * ck;
  ss = si * sk;

  std::array<double, 4> q;
  if (repetition)
  {
    q.at(i) = cj * (cs + sc);
    q.at(k) = sj * (cs - sc);
    q.at(j) = sj * (cc + ss);
    q.at(0) = cj * (cc - ss);
  }
  else
  {
    q.at(i) = cj * sc - sj * cs;
    q.at(k) = cj * cs - sj * sc;
    q.at(j) = cj * ss + sj * cc;
    q.at(0) = cj * cc + sj * ss;
  }

  if (parity)
  {
    q.at(j) *= -1.0;
  }
  quat->w = q.at(0);
  quat->x = q.at(1);
  quat->y = q.at(2);
  quat->z = q.at(3);
  return 0;
}

int Transformations::matrixFromQuaternion(const Quaternion& quat, Matrix4x4* matrix)
{
  std::array<double, 4> q = { quat.w, quat.x, quat.y, quat.z };
  Matrix4x4* M = matrix;
  double n = std::sqrt(q[0] * q[0] + q[1] * q[1] + q[2] * q[2] + q[3] * q[3]);

  if (n < std::numeric_limits<double>::epsilon())
  {
    /* return identity matrix */
    for (auto it = M->begin(); it != M->end(); ++it)
    {
      it->fill(0.0);
    }
    (*M)[0][0] = (*M)[1][1] = (*M)[2][2] = (*M)[3][3] = 1.0;
  }
  else
  {
    q[0] /= n;
    q[1] /= n;
    q[2] /= n;
    q[3] /= n;
    {
      double x2 = q[1] + q[1];
      double y2 = q[2] + q[2];
      double z2 = q[3] + q[3];
      {
        double xx2 = q[1] * x2;
        double yy2 = q[2] * y2;
        double zz2 = q[3] * z2;
        (*M)[0][0] = 1.0 - yy2 - zz2;
        (*M)[1][1] = 1.0 - xx2 - zz2;
        (*M)[2][2] = 1.0 - xx2 - yy2;
      }
      {
        double yz2 = q[2] * z2;
        double wx2 = q[0] * x2;
        (*M)[1][2] = yz2 - wx2;
        (*M)[2][1] = yz2 + wx2;
      }
      {
        double xy2 = q[1] * y2;
        double wz2 = q[0] * z2;
        (*M)[0][1] = xy2 - wz2;
        (*M)[1][0] = xy2 + wz2;
      }
      {
        double xz2 = q[1] * z2;
        double wy2 = q[0] * y2;
        (*M)[2][0] = xz2 - wy2;
        (*M)[0][2] = xz2 + wy2;
      }
      (*M)[0][3] = (*M)[1][3] = (*M)[2][3] = (*M)[3][0] = (*M)[3][1] = (*M)[3][2] = 0.0;
      (*M)[3][3] = 1.0;
    }
  }
  return 0;
}

int Transformations::eulerFromQuaternion(const Quaternion& quat, const enum RotationConvention& axes, double* roll,
                                         double* pitch, double* yaw)
{
  Matrix4x4 m;
  int rt = matrixFromQuaternion(quat, &m);
  if (0 != rt)
    return rt;
  return eulerFromMatrix(m, axes, roll, pitch, yaw);
}

int Transformations::eulerFromMatrix(const Matrix4x4& matrix, const enum RotationConvention& axes, double* roll,
                                     double* pitch, double* yaw)
{
  if (!roll || !pitch || !yaw)
    return -1;

  double ai = 0.0;
  double aj = 0.0;
  double ak = 0.0;

  std::array<int, 4> tuple;
  try
  {
    tuple = axis_to_tuple_.at(axes);
  }
  catch (const std::exception& e)
  {
    return -1;
  }

  int firstaxis = tuple.at(0);
  int parity = tuple.at(1);
  int repetition = tuple.at(2);
  int frame = tuple.at(3);

  int i = firstaxis;
  int j = next_axis_[i + parity];
  int k = next_axis_[i - parity + 1];
  double x, y, t;
  Matrix4x4 M = matrix;

  if (repetition)
  {
    x = M[i][j];
    y = M[i][k];
    t = sqrt(x * x + y * y);
    if (t > std::numeric_limits<double>::epsilon())
    {
      ai = atan2(M[i][j], M[i][k]);
      aj = atan2(t, M[i][i]);
      ak = atan2(M[j][i], -M[k][i]);
    }
    else
    {
      ai = atan2(-M[j][k], M[j][j]);
      aj = atan2(t, M[i][i]);
    }
  }
  else
  {
    x = M[i][i];
    y = M[j][i];
    t = sqrt(x * x + y * y);
    if (t > std::numeric_limits<double>::epsilon())
    {
      ai = atan2(M[k][j], M[k][k]);
      aj = atan2(-M[k][i], t);
      ak = atan2(M[j][i], M[i][i]);
    }
    else
    {
      ai = atan2(-M[j][k], M[j][j]);
      aj = atan2(-M[k][i], t);
    }
  }
  if (parity)
  {
    ai = -ai;
    aj = -aj;
    ak = -ak;
  }
  if (frame)
  {
    t = ai;
    ai = ak;
    ak = t;
  }

  *roll = ai;
  *pitch = aj;
  *yaw = ak;

  return 0;
}

}  // namespace utils
}  // namespace nus_unity_socket
