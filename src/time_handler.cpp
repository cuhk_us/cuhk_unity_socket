// Copyright 2015-2017 Yuchao Hu

#include "nus_unity_socket/time_handler.h"

namespace nus_unity_socket
{
bool TimeHandler::initialized = false;
float TimeHandler::init_unity_time = 0;
ros::Time TimeHandler::init_ros_time = ros::Time(0);

void TimeHandler::SetInitUnityTime(float unity_time)
{
  init_unity_time = unity_time;
  init_ros_time = ros::Time::now();
  initialized = true;
}

bool TimeHandler::IsInitialized()
{
  return initialized;
}

ros::Time TimeHandler::GetTime(float unity_time)
{
  if (!initialized)
  {
    return ros::Time::now();
  }
  else
  {
    return init_ros_time + ros::Duration(unity_time - init_unity_time);
  }
}

}  // namespace nus_unity_socket
