// Copyright 2015-2017 Yuchao Hu

#include "nus_unity_socket/message_handler_laser3d.h"
#include <pcl_conversions/pcl_conversions.h>
#include <sensor_msgs/PointCloud2.h>
#include <cmath>
#include "nus_unity_socket/time_handler.h"

namespace nus_unity_socket
{
MessageHandlerLaser3D::MessageHandlerLaser3D(ros::NodeHandle& node)
{
  sensor_laser3d_pointcloud_pub_ = node.advertise<sensor_msgs::PointCloud2>("/hil/sensor/laser3d_pointcloud", 10);
}

void MessageHandlerLaser3D::ParseChar(const uint8_t& cp)
{
}

void MessageHandlerLaser3D::ParseMessage(const void* message_ptr, std::size_t bytes_transferred)
{
  if (!laser_scan_msg_.ParseFromArray(message_ptr, bytes_transferred))
  {
    return;
  }
  HandleMessage(laser_scan_msg_);
}

void MessageHandlerLaser3D::HandleMessage(const nus_protocol::LaserScan& message)
{
  if (sensor_laser3d_pointcloud_pub_.getNumSubscribers() > 0)
  {
    ros::Time time_now = TimeHandler::GetTime(message.stamp());
    PointCloudT::Ptr pcl_cloud_ptr = boost::shared_ptr<PointCloudT>(new PointCloudT());
    pcl_cloud_ptr->points.reserve(message.scan_size());
    for (int i = 0; i < 3; ++i)
    {
      FillPointCloud(message, pcl_cloud_ptr, i);
    }
    pcl_conversions::toPCL(time_now, pcl_cloud_ptr->header.stamp);
    pcl_cloud_ptr->header.frame_id = "laser3d";
    pcl_cloud_ptr->width = pcl_cloud_ptr->points.size();
    pcl_cloud_ptr->height = 1;
    pcl_cloud_ptr->is_dense = false;
    sensor_msgs::PointCloud2 cloud_msg;
    pcl::toROSMsg(*pcl_cloud_ptr, cloud_msg);
    sensor_laser3d_pointcloud_pub_.publish(cloud_msg);
  }
}

void MessageHandlerLaser3D::FillPointCloud(const nus_protocol::LaserScan& message,
                                           const PointCloudT::Ptr& pcl_cloud_ptr, int index)
{
  int points_num_horizontal = 300;
  int points_num_vertical = 16;
  float laser_fov_vertical = 30.0;     // degrees
  float laser_fov_horizontal = 120.0;  // degrees
  float laser_res_vertical = 2.0;      // degrees
  float laser_res_horizontal = 0.4;    // defrees
  int offset_index = points_num_horizontal * points_num_vertical * index;
  float offset_rad = (laser_fov_horizontal * index - 180.0) / 180.0 * M_PI;
  float step_rad = laser_res_horizontal / 180.0 * M_PI;
  for (int i = 0; i < points_num_vertical; ++i)
  {
    float theta = (laser_fov_vertical / 2.0 - laser_res_vertical * i) / 180.0 * M_PI;
    float sin_theta = sin(theta);
    float cos_theta = cos(theta);
    int offset_j = offset_index + points_num_horizontal * i;
    for (int j = 0; j < points_num_horizontal; ++j)
    {
      PointT p;
      p.z = message.scan(offset_j + j) * sin_theta;
      float depth = message.scan(offset_j + j) * cos_theta;
      float angle = offset_rad + step_rad * j;
      p.x = -depth * cos(angle);  // left handed to right handed
      p.y = depth * sin(angle);
      pcl_cloud_ptr->points.push_back(p);
    }
  }
}

}  // namespace nus_unity_socket
