// Copyright 2015-2017 Yuchao Hu

#include "nus_unity_socket/message_handler_mavlink.h"

#ifndef MAVLINK_FRAMING_OK
#define MAVLINK_FRAMING_OK 1
#endif

namespace nus_unity_socket
{
MessageHandlerMavlink::MessageHandlerMavlink(ros::NodeHandle& node) : channel(MAVLINK_COMM_0)
{
}

void MessageHandlerMavlink::ParseChar(const uint8_t& cp)
{
  if (MAVLINK_FRAMING_OK == mavlink_parse_char(channel, cp, &received_message, &received_status))
  {
    HandleMessage(received_message, received_message.sysid, received_message.compid);
  }
}

void MessageHandlerMavlink::ParseMessage(const void* message_ptr, std::size_t bytes_transferred)
{
}

void MessageHandlerMavlink::HandleMessage(const mavlink_message_t& message, uint8_t sysid, uint8_t compid)
{
  switch (message.msgid)
  {
    case MAVLINK_MSG_ID_HIL_RC_INPUTS_RAW:
    {  // #92
      HandleHilRcInputRaw(message, sysid, compid);
      break;
    }
    case MAVLINK_MSG_ID_HIL_SENSOR:
    {  // #107
      HandleHilSensor(message, sysid, compid);
      break;
    }
    case MAVLINK_MSG_ID_HIL_GPS:
    {  // #113
      HandleHilGps(message, sysid, compid);
      break;
    }
    case MAVLINK_MSG_ID_HIL_OPTICAL_FLOW:
    {  // #114
      HandleHilOpticalFlow(message, sysid, compid);
      break;
    }
    case MAVLINK_MSG_ID_HIL_STATE_QUATERNION:
    {  // #115
      HandleHilStateQuaternion(message, sysid, compid);
      break;
    }
    default:
    {
      // warn
      break;
    }
  }
}

void MessageHandlerMavlink::HandleHilRcInputRaw(const mavlink_message_t& msg, const uint8_t& sysid,
                                                const uint8_t& compid)
{
}

void MessageHandlerMavlink::HandleHilSensor(const mavlink_message_t& msg, const uint8_t& sysid, const uint8_t& compid)
{
}

void MessageHandlerMavlink::HandleHilGps(const mavlink_message_t& msg, const uint8_t& sysid, const uint8_t& compid)
{
}

void MessageHandlerMavlink::HandleHilOpticalFlow(const mavlink_message_t& msg, const uint8_t& sysid,
                                                 const uint8_t& compid)
{
}

void MessageHandlerMavlink::HandleHilStateQuaternion(const mavlink_message_t& msg, const uint8_t& sysid,
                                                     const uint8_t& compid)
{
}

}  // namespace nus_unity_socket
