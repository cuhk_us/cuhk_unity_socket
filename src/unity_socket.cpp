// Copyright 2015-2017 Yuchao Hu

#include "nus_unity_socket/unity_socket.h"
#include <string>
#include "mavros/utils.h"
#include "nus_unity_socket/message_handler_basic.h"
#include "nus_unity_socket/message_handler_image.h"
#include "nus_unity_socket/message_handler_laser2d.h"
#include "nus_unity_socket/message_handler_laser3d.h"
#include "nus_unity_socket/message_handler_mavlink.h"
#include "nus_unity_socket/message_handler_stereo.h"
//#include "nus_unity_socket/message_handler_AI.h"
#include "nus_unity_socket/message_parser.h"

namespace nus_unity_socket
{
UnitySocket::UnitySocket()
{
  ros::NodeHandle private_nh_("~");

  // parameter
  private_nh_.param("server_ip", p_server_ip_, std::string("192.168.1.100")); //172.16.142.34
  private_nh_.param("server_port_basic", p_server_port_basic_, 26000);
  private_nh_.param("server_port_image", p_server_port_image_, 26500);
  private_nh_.param("server_port_stereo", p_server_port_stereo_, 26510);
  private_nh_.param("server_port_laser2d", p_server_port_laser2d_, 26600);
  private_nh_.param("server_port_laser3d", p_server_port_laser3d_, 26700);
  private_nh_.param("server_port_mavlink", p_server_port_mavlink_, 27000);
  //private_nh_.param("server_port_AI", p_server_port_AI_, 28000);

  std::shared_ptr<MessageHandler> message_handler_basic_ptr = std::make_shared<MessageHandlerBasic>(node_);
  std::shared_ptr<MessageHandler> message_handler_laser2d_ptr = std::make_shared<MessageHandlerLaser2D>(node_);
  std::shared_ptr<MessageHandler> message_handler_laser3d_ptr = std::make_shared<MessageHandlerLaser3D>(node_);
  std::shared_ptr<MessageHandler> message_handler_image_ptr = std::make_shared<MessageHandlerImage>(node_);
  std::shared_ptr<MessageHandler> message_handler_stereo_ptr = std::make_shared<MessageHandlerStereo>(node_);
  //std::shared_ptr<MessageHandler> message_handler_AI_ptr = std::make_shared<MessageHandlerAI>(node_);

  udp_client_basic_ptr_ = std::make_shared<UdpClient>(p_server_ip_, p_server_port_basic_, message_handler_basic_ptr);
  udp_client_laser2d_ptr_ =
      std::make_shared<UdpClient>(p_server_ip_, p_server_port_laser2d_, message_handler_laser2d_ptr);
  udp_client_laser3d_ptr_ =
      std::make_shared<UdpClient>(p_server_ip_, p_server_port_laser3d_, message_handler_laser3d_ptr);
  udp_client_image_ptr_ = std::make_shared<UdpClient>(p_server_ip_, p_server_port_image_, message_handler_image_ptr);
  udp_client_stereo_ptr_ = std::make_shared<UdpClient>(p_server_ip_, p_server_port_stereo_, message_handler_stereo_ptr);
  //udp_client_AI_ptr_ = std::make_shared<UdpClient>(p_server_ip_, p_server_port_AI_, message_handler_AI_ptr);

  udp_client_basic_ptr_->Connect();
  udp_client_laser2d_ptr_->Connect();
  udp_client_laser3d_ptr_->Connect();
  udp_client_image_ptr_->Connect();
  udp_client_stereo_ptr_->Connect();
  //udp_client_AI_ptr_->Connect();

  // unity mavlink client
  if (p_server_port_mavlink_ > 0)
  {
    mavlink_sub_ = node_.subscribe("/mavlink/from_pixhawk", 2, &UnitySocket::MavlinkCallback, this);
    std::shared_ptr<MessageHandler> message_handler_mavlink_ptr = std::make_shared<MessageHandlerMavlink>(node_);
    udp_client_mavlink_ptr_ =
        std::make_shared<UdpClient>(p_server_ip_, p_server_port_mavlink_, message_handler_mavlink_ptr);
    udp_client_mavlink_ptr_->Connect();
  }
}

UnitySocket::~UnitySocket()
{
}

void UnitySocket::MavlinkCallback(const mavros::Mavlink::ConstPtr& msg_ptr)
{
  switch (msg_ptr->msgid)
  {
    case MAVLINK_MSG_ID_HIL_ACTUATOR_CONTROLS:
    {
      mavlink_message_t mmsg;
      if (mavutils::copy_ros_to_mavlink(msg_ptr, mmsg))
      {
        mavlink_finalize_message(&mmsg, 1, 2, MAVLINK_MSG_ID_HIL_ACTUATOR_CONTROLS_MIN_LEN,
                                 MAVLINK_MSG_ID_HIL_ACTUATOR_CONTROLS_LEN, MAVLINK_MSG_ID_HIL_ACTUATOR_CONTROLS_CRC);
        udp_client_mavlink_ptr_->Send(&mmsg, sizeof(mmsg));
      }
      break;
    }
    default:
    {
      break;
    }
  }
}

}  // namespace nus_unity_socket
