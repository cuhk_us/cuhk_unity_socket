// Copyright 2015-2017 Yuchao Hu

#include "nus_unity_socket/message_handler_stereo.h"
#include <cv_bridge/cv_bridge.h>
#include <image_transport/image_transport.h>
#include <sensor_msgs/CameraInfo.h>
#include <sensor_msgs/Image.h>
#include <opencv2/highgui/highgui.hpp>
#include <vector>
#include "nus_unity_socket/time_handler.h"

namespace nus_unity_socket
{
MessageHandlerStereo::MessageHandlerStereo(ros::NodeHandle& node) : frag_seq_(0), frag_id_(-1), frag_num_(0) // all -1 by default
{
  sensor_stereo_left_image_raw_pub_ = node.advertise<sensor_msgs::Image>("/hil/sensor/stereo/left/image_raw", 30);
  sensor_stereo_left_camera_info_pub_ = node.advertise<sensor_msgs::CameraInfo>("/hil/sensor/stereo/left/camera_info", 30);
  sensor_stereo_right_image_raw_pub_ = node.advertise<sensor_msgs::Image>("/hil/sensor/stereo/right/image_raw", 30);
  sensor_stereo_right_camera_info_pub_ = node.advertise<sensor_msgs::CameraInfo>("/hil/sensor/stereo/right/camera_info", 30);
}

void MessageHandlerStereo::ParseChar(const uint8_t& cp)
{
}

void MessageHandlerStereo::ParseMessage(const void* message_ptr, std::size_t bytes_transferred)
{
  if (!frag_msg_.ParseFromArray(message_ptr, bytes_transferred))
  {
    return;
  }
  HandleMessage(frag_msg_);
}

void MessageHandlerStereo::HandleMessage(const nus_protocol::Fragmentation& message)
{
  if (message.seq() >= 0 && message.fragment_id() >= 0 && message.fragment_number() > 0 &&
      message.fragment_id() < message.fragment_number())
  {
    if (message.seq() != frag_seq_ && 0 == message.fragment_id())
    {
    // create new seq bytes
      frag_data_.clear();
      frag_data_ += message.data();
    }
    else if (message.seq() == frag_seq_ && message.fragment_id() == frag_id_ + 1 &&
             message.fragment_number() == frag_num_)
    {
 // Append msg
      frag_data_ += message.data();
    }

    if (message.fragment_id() + 1 == message.fragment_number())
    {
	// last message in seq

      if ( (message.seq() == frag_seq_ && message.fragment_number() >  1) ||  // data fragmented
	    	 (message.seq() != frag_seq_ && message.fragment_number() == 1) )	// data not fragmented
         // (message.seq() == frag_seq_ && message.fragment_id() + 1 == message.fragment_number())
      {
        if (stereo_msg_.ParseFromString(frag_data_))
        {
          HandleMessage(stereo_msg_);
        }
      }
    }

    frag_seq_ = message.seq();
    frag_id_ = message.fragment_id();
    frag_num_ = message.fragment_number();
  }
}

void MessageHandlerStereo::HandleMessage(const nus_protocol::Stereo& message)
{
  if (sensor_stereo_left_image_raw_pub_.getNumSubscribers() > 0 || sensor_stereo_right_image_raw_pub_.getNumSubscribers() > 0)
  {
    ros::Time time_now = TimeHandler::GetTime(message.stamp());    
    if (message.has_image_left_data())
    {
      std::vector<uchar> buffer(message.image_left_data().begin(), message.image_left_data().end());
      // printf("Hey Y'all!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n"); //junji
      cv::Mat image_frame = cv::imdecode(buffer, cv::IMREAD_COLOR);
      if (!image_frame.empty())
      {
        sensor_msgs::ImagePtr img_ptr = cv_bridge::CvImage(std_msgs::Header(), "bgr8", image_frame).toImageMsg();
        img_ptr->header.stamp = time_now;
        sensor_stereo_left_image_raw_pub_.publish(img_ptr);

        // below is for camera info
        double fy = image_frame.rows / 2.0 / std::tan(message.vertical_fov() / 2.0);
        double fx = fy;
        double cx = image_frame.cols / 2.0;
        double cy = image_frame.rows / 2.0;
        double tx = 0;
        double ty = 0;
        sensor_msgs::CameraInfo camera_info;
        camera_info.header.stamp = time_now;
        camera_info.height = image_frame.rows;
        camera_info.width = image_frame.cols;
        camera_info.distortion_model = "plumb_bob";
        camera_info.D.resize(5);
        std::fill(camera_info.D.begin(), camera_info.D.end(), 0);
        camera_info.K.fill(0);
        camera_info.K[0] = fx;
        camera_info.K[4] = fy;
        camera_info.K[8] = 1;
        camera_info.K[2] = cx;
        camera_info.K[5] = cy;
        camera_info.R.fill(0);
        camera_info.R[0] = camera_info.R[4] = camera_info.R[8] = 1;
        camera_info.P.fill(0);
        camera_info.P[0] = fx;
        camera_info.P[5] = fy;
        camera_info.P[2] = cx;
        camera_info.P[6] = cy;
        camera_info.P[3] = tx;
        camera_info.P[7] = ty;
        camera_info.P[10] = 1;
        sensor_stereo_left_camera_info_pub_.publish(camera_info);
      }
    }
    
    if (message.has_image_right_data()) //message.has_image_right_data()
    {
      std::vector<uchar> buffer(message.image_right_data().begin(), message.image_right_data().end());
      cv::Mat image_frame = cv::imdecode(buffer, cv::IMREAD_COLOR);
      if (!image_frame.empty())
      {
        sensor_msgs::ImagePtr img_ptr = cv_bridge::CvImage(std_msgs::Header(), "bgr8", image_frame).toImageMsg();
        img_ptr->header.stamp = time_now;
        sensor_stereo_right_image_raw_pub_.publish(img_ptr);

        // below is for camera info
        double fy = image_frame.rows / 2.0 / std::tan(message.vertical_fov() / 2.0);
        double fx = fy;
        double cx = image_frame.cols / 2.0;
        double cy = image_frame.rows / 2.0;
        double tx = -fx * message.baseline();
        double ty = 0;
        sensor_msgs::CameraInfo camera_info;
        camera_info.header.stamp = time_now;
        camera_info.height = image_frame.rows;
        camera_info.width = image_frame.cols;
        camera_info.distortion_model = "plumb_bob";
        camera_info.D.resize(5);
        std::fill(camera_info.D.begin(), camera_info.D.end(), 0);
        camera_info.K.fill(0);
        camera_info.K[0] = fx;
        camera_info.K[4] = fy;
        camera_info.K[8] = 1;
        camera_info.K[2] = cx;
        camera_info.K[5] = cy;
        camera_info.R.fill(0);
        camera_info.R[0] = camera_info.R[4] = camera_info.R[8] = 1;
        camera_info.P.fill(0);
        camera_info.P[0] = fx;
        camera_info.P[5] = fy;
        camera_info.P[2] = cx;
        camera_info.P[6] = cy;
        camera_info.P[3] = tx;
        camera_info.P[7] = ty;
        camera_info.P[10] = 1;
        sensor_stereo_right_camera_info_pub_.publish(camera_info);
      }
    }
  }
}

} // namespace nus_unity_socket
