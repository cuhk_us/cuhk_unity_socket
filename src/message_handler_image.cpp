// Copyright 2015-2017 Yuchao Hu

#include "nus_unity_socket/message_handler_image.h"
#include <cv_bridge/cv_bridge.h>
#include <image_transport/image_transport.h>
#include <sensor_msgs/Image.h>
#include <opencv2/highgui/highgui.hpp>
#include <vector>
#include "nus_unity_socket/time_handler.h"

namespace nus_unity_socket
{
MessageHandlerImage::MessageHandlerImage(ros::NodeHandle& node)
{
  sensor_image_pub_ = node.advertise<sensor_msgs::Image>("/hil/sensor/image", 30);
}

void MessageHandlerImage::ParseChar(const uint8_t& cp)
{
}

void MessageHandlerImage::ParseMessage(const void* message_ptr, std::size_t bytes_transferred)
{
  if (!frag_msg_.ParseFromArray(message_ptr, bytes_transferred))
  {
    return;
  }
  HandleMessage(frag_msg_);
}

void MessageHandlerImage::HandleMessage(const nus_protocol::Fragmentation& message)
{
  if (message.seq() >= 0 && message.fragment_id() >= 0 && message.fragment_number() > 0 &&
      message.fragment_id() < message.fragment_number())
  {
    if (message.seq() != frag_seq_ && 0 == message.fragment_id())
    {
    // create new seq bytes
      frag_data_.clear();
      frag_data_ += message.data();
    }
    else if (message.seq() == frag_seq_ && message.fragment_id() == frag_id_ + 1 &&
             message.fragment_number() == frag_num_)
    {
	// Append msg
      frag_data_ += message.data();
    }

    if (message.fragment_id() + 1 == message.fragment_number())
    {
	// last message in seq  				
    	if ( (message.seq() == frag_seq_ && message.fragment_number() >  1) ||  // data fragmented
	    	 (message.seq() != frag_seq_ && message.fragment_number() == 1) )	// data not fragmented
		{
	    	if (image_msg_.ParseFromString(frag_data_))
	   		{
       			HandleMessage(image_msg_);
    		}
		}
    }

    frag_seq_ = message.seq();
    frag_id_ = message.fragment_id();
    frag_num_ = message.fragment_number();
  }
}

cv_bridge::CvImage img_bridge;
sensor_msgs::Image img_msg; // >>
void MessageHandlerImage::HandleMessage(const nus_protocol::Image& message)
{
  if (sensor_image_pub_.getNumSubscribers() > 0)
  {
    ros::Time time_now = TimeHandler::GetTime(message.stamp());
    std::vector<uchar> buffer(message.image_data().begin(), message.image_data().end());
    if(buffer.empty())
    return;
    cv::Mat image_frame = cv::imdecode(buffer, cv::IMREAD_UNCHANGED);
    cv::cvtColor(image_frame,image_frame,CV_BGR2GRAY);
    cv::Mat image_frame_f;
    image_frame.convertTo(image_frame_f, CV_32FC1);
    image_frame_f*=0.13;
    std_msgs::Header header;
    header.stamp = time_now;
    img_bridge = cv_bridge::CvImage(header, sensor_msgs::image_encodings::TYPE_32FC1, image_frame_f);
    img_bridge.toImageMsg(img_msg);
    //publish disparity image
    // sensor_msgs::ImagePtr img_ptr = cv_bridge::CvImage(std_msgs::Header(), "mono8", image_frame).toImageMsg();
    // sensor_image_pub_.publish(img_ptr);


    //publish depth map
//     float scale = 408 * 0.15;    //f*b/d
//     sensor_msgs::ImagePtr depthMessage = boost::make_shared<sensor_msgs::Image>();
//     // depthMessage->header.stamp = ;
//     // depthMessage->header.frame_id = "map";
//     depthMessage->height = 480;
//     depthMessage->width = 640;
//     depthMessage->step = depthMessage->width * sizeof(float); //uint16_t
// //    depthMessage->encoding = sensor_msgs::image_encodings::MONO16;
//     size_t size = depthMessage->step * depthMessage->height;
//     depthMessage->data.resize(size);
//     float* data = (float*)(&depthMessage->data[0]);
//     int dataSize = depthMessage->width * depthMessage->height;
//     float ratio=4.44;//5.08;//4.572;
//     for (int i = 0; i < dataSize; i++) {
//         *(data++) =  ratio*scale/image_frame.at<uchar>(i/depthMessage->width, i%depthMessage->width);    // in m
//     }
    sensor_image_pub_.publish(img_msg);
  }
}

}
// namespace nus_unity_socket
//void StereoUpdater::preProcess(const sensor_msgs::Image::ConstPtr &depthPoint,
//                               const sensor_msgs::Image::ConstPtr &left_color,
//                               const sensor_msgs::Image::ConstPtr &confidence_ptr)
//{
//  cv_bridge::CvImagePtr depth_cv_temp,color_cv_temp,confi_cv_temp;
//      cv::Mat depth_cv,color_cv,confi_cv;
//     try
//     {
//       depth_cv_temp = cv_bridge::toCvCopy(depthPoint, sensor_msgs::image_encodings::TYPE_64FC1);
//     }
//     catch (cv_bridge::Exception& e)
//     {
//       ROS_ERROR("cv_bridge exception: %s", e.what());
//       return;
//     }
//  try
//  {
//    color_cv_temp = cv_bridge::toCvCopy(left_color, sensor_msgs::image_encodings::BGR8);
//  }
//  catch (cv_bridge::Exception& e)
//  {
//    ROS_ERROR("cv_bridge exception: %s", e.what());
//    return;
//  }
//  try
//  {
//    confi_cv_temp = cv_bridge::toCvCopy(confidence_ptr, sensor_msgs::image_encodings::TYPE_64FC1);
//  }
//  catch (cv_bridge::Exception& e)
//  {
//    ROS_ERROR("cv_bridge exception: %s", e.what());
//    return;
//  }

//  cv::resize(depth_cv_temp->image,depth_cv,dist_size);
//  cv::resize(color_cv_temp->image,color_cv,dist_size);
//  cv::resize(confi_cv_temp->image,confi_cv,dist_size);

//  depth_img_d.setDevData(reinterpret_cast<float *>(depth_cv.data));
//  color_img_d.setDevData(reinterpret_cast<float *>(color_cv.data));
//  confi_img_d.setDevData(reinterpret_cast<float *>(confi_cv.data));

//}
