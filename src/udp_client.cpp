// Copyright 2015-2017 Yuchao Hu

#include "nus_unity_socket/udp_client.h"
#include <ros/ros.h>
#include <cstring>
#include <string>

namespace nus_unity_socket
{
UdpClient::UdpClient(const std::string& remote_ip, const int& remote_port,
                     const std::shared_ptr<MessageHandler>& message_handler_ptr)
  : io_service_()
  , io_work_ptr_(new boost::asio::io_service::work(io_service_))
  , udp_socket_(io_service_)
  , message_handler_ptr_(message_handler_ptr)
{
  boost::asio::ip::address ip_address;
  ip_address = ip_address.from_string(remote_ip);
  udp_remote_endpoint_.address(ip_address);
  udp_remote_endpoint_.port(remote_port);
  udp_bind_endpoint_.port(remote_port + 1);
}

UdpClient::~UdpClient()
{
  Close();
}

void UdpClient::Connect()
{
  std::cout << "binding: " << udp_bind_endpoint_ << std::endl;
  udp_socket_.open(boost::asio::ip::udp::v4());
  udp_socket_.bind(udp_bind_endpoint_);
  std::cout << "binded: " << udp_bind_endpoint_ << std::endl;

  auto shared_this = shared_from_this();
  io_service_.post(std::bind(&UdpClient::Receive, shared_this));
  io_thread_ = boost::thread(&UdpClient::RunThread, shared_this);
}

void UdpClient::Disconnect()
{
  Close();
}

void UdpClient::Close()
{
  io_work_ptr_.reset();
  io_service_.stop();
  udp_socket_.close();

  if (io_thread_.joinable())
    io_thread_.join();
}

void UdpClient::Receive()
{
  udp_socket_.async_receive(boost::asio::buffer(rx_buffer_),
                            boost::bind(&UdpClient::ReadHandler, shared_from_this(), boost::asio::placeholders::error,
                                        boost::asio::placeholders::bytes_transferred));
}

void UdpClient::ReadHandler(const boost::system::error_code& error, std::size_t bytes_transferred)
{
  if (error)
  {
    return;
  }

  message_handler_ptr_->ParseMessage(rx_buffer_.data(), bytes_transferred);

  Receive();
}

void UdpClient::Send(const void* bufs, const size_t& count)
{
  udp_socket_.send_to(boost::asio::buffer(bufs, count), udp_remote_endpoint_);
}

void UdpClient::RunThread()
{
  io_service_.run();
}

}  // namespace nus_unity_socket
