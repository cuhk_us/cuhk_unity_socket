// Copyright 2015-2017 Yuchao Hu

#include <ros/ros.h>
#include "nus_unity_socket/unity_socket.h"

int main(int argc, char** argv)
{
  ros::init(argc, argv, "nus_unity_socket");

  nus_unity_socket::UnitySocket s;

  ros::spin();

  return 0;
}
