// Copyright 2017 Yuchao Hu

#include "nus_unity_socket/utils.h"
#include <gtest/gtest.h>

using namespace nus_unity_socket::utils;

TEST(Transformations, quaternionFromEulerRXYZ)
{
  double roll = 0.2;
  double pitch = 0.3;
  double yaw = 0.7;
  Transformations::RotationConvention axes = Transformations::RotationConvention::kRXYZ;
  Transformations::Quaternion qurt;
  int rt = Transformations::quaternionFromEuler(roll, pitch, yaw, axes, &qurt);
  EXPECT_EQ(0, rt);
  EXPECT_NEAR(0.9191, qurt.w, 1e-4);
  EXPECT_NEAR(0.1437, qurt.x, 1e-4);
  EXPECT_NEAR(0.1058, qurt.y, 1e-4);
  EXPECT_NEAR(0.3514, qurt.z, 1e-4);
}

TEST(Transformations, quaternionFromEulerRZYX)
{
  double roll = 0.2;
  double pitch = 0.3;
  double yaw = 0.7;
  Transformations::RotationConvention axes = Transformations::RotationConvention::kRZYX;
  Transformations::Quaternion qurt;
  int rt = Transformations::quaternionFromEuler(roll, pitch, yaw, axes, &qurt);
  EXPECT_EQ(0, rt);
  EXPECT_NEAR(0.9293, qurt.w, 1e-4);
  EXPECT_NEAR(0.3233, qurt.x, 1e-4);
  EXPECT_NEAR(0.1735, qurt.y, 1e-4);
  EXPECT_NEAR(0.0417, qurt.z, 1e-4);
}

TEST(Transformations, eulerFromQuaternionRXYZ)
{
  double roll, pitch, yaw;
  Transformations::RotationConvention axes = Transformations::RotationConvention::kRXYZ;
  Transformations::Quaternion qurt;
  qurt.w = 0.9191;
  qurt.x = 0.1437;
  qurt.y = 0.1058;
  qurt.z = 0.3514;
  int rt = Transformations::eulerFromQuaternion(qurt, axes, &roll, &pitch, &yaw);
  EXPECT_EQ(0, rt);
  EXPECT_NEAR(0.2, roll, 1e-3);
  EXPECT_NEAR(0.3, pitch, 1e-3);
  EXPECT_NEAR(0.7, yaw, 1e-3);
}

TEST(Transformations, eulerFromQuaternionRZYX)
{
  double roll, pitch, yaw;
  Transformations::RotationConvention axes = Transformations::RotationConvention::kRZYX;
  Transformations::Quaternion qurt;
  qurt.w = 0.9293;
  qurt.x = 0.3233;
  qurt.y = 0.1735;
  qurt.z = 0.0417;
  int rt = Transformations::eulerFromQuaternion(qurt, axes, &roll, &pitch, &yaw);
  EXPECT_EQ(0, rt);
  EXPECT_NEAR(0.2, roll, 1e-3);
  EXPECT_NEAR(0.3, pitch, 1e-3);
  EXPECT_NEAR(0.7, yaw, 1e-3);
}

int main(int argc, char** argv)
{
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
